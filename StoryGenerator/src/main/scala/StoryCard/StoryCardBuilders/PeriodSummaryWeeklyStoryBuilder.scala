package scala.StoryCard.StoryCardBuilders

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard.StoryCardBuilders.{AbstractStoryBuilder, AbstractStoryCardBuilderObject, PeriodSummaryIntervalStoryBuilder}

class PeriodSummaryWeeklyStoryBuilder(storiesBackend: ValhallaApi) extends PeriodSummaryIntervalStoryBuilder(storiesBackend) {
  override val storyType: String = s"period-summary-weekly"

  override def subtractStoryIntervalFromDate(date: LocalDate): LocalDate = {
     date.minusWeeks(1)
  }
}

object PeriodSummaryWeeklyStoryBuilder extends AbstractStoryCardBuilderObject {

  override def storyIsValid(storeId: Int, date: LocalDate): Boolean = {
    true
  }

  def getBuilder(storiesBackend: ValhallaApi): AbstractStoryBuilder = {
    new PeriodSummaryWeeklyStoryBuilder(storiesBackend)
  }
}