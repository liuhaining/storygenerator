package StoryGenerator.Preconditions

import java.time.{DayOfWeek, LocalDate}

import StoryBackend.ValhallaApi
import org.apache.log4j.Logger

import scala.collection.mutable.ListBuffer

/**
  * Created by rkilgore on 8/21/17.
  */

abstract class AbstractStoryPrecondition(var initialStores: List[Int]) {
  val log: Logger = Logger.getLogger(getClass.getName)
  var stores = new ListBuffer[Int]
  stores.appendAll(initialStores)

  val storyType: String
  val storiesBackend = new ValhallaApi

  val storesBlacklist: ListBuffer[Int] = new ListBuffer[Int]
  storesBlacklist.appendAll(getStoresWithStoriesAlreadyGenerated)

  def getStoresWithStoriesAlreadyGenerated: List[Int] = {
    val storesWithStoriesGeneratedOnTargetDate = storiesBackend.getStoresWithStoriesGeneratedOnTargetDate(storyType)
    log.info(s"[Precondition]: Stores excluded from type $storyType: ${storesWithStoriesGeneratedOnTargetDate.toString}")
    storesWithStoriesGeneratedOnTargetDate
  }

  def getStoresForStoryRender: List[Int] = {
    stores = stores.filterNot(storesBlacklist.toList.contains)
    log.info(s"[Precondition]: Stores included in type $storyType: ${stores.toString}")
    stores.toList
  }
}

object StoryPreconditions {
  def filterOutInvalidStores(storyType: String, stores: List[Int], targetDate: LocalDate): List[Int] = {
    storyType match {
      case "closing-comparison" => new ClosingComparisonStoryPreconditions(stores).getStoresForStoryRender
      case "period-best-day" => new PeriodBestDayStoryPreconditions(stores, targetDate).getStoresForStoryRender
      case "period-summary-weekly" => new PeriodSummaryWeeklyStoryPreconditions(stores, targetDate).getStoresForStoryRender
      case "period-summary-monthly" => new PeriodSummaryMonthlyStoryPreconditions(stores, targetDate).getStoresForStoryRender
      case "period-summary-quarterly" => new PeriodSummaryQuarterlyStoryPreconditions(stores, targetDate).getStoresForStoryRender
      case "upcoming-holiday" => new UpcomingHolidayPreconditions(stores, targetDate).getStoresForStoryRender
      case "last-k-days" => new LastKDaysStoryPreconditions(stores, targetDate).getStoresForStoryRender
      case _ => throw new Exception("Invalid story type")
    }
  }
}