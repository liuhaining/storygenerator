package StoryCard

import Utils.JsonSerializable
import com.fasterxml.jackson.annotation.JsonIgnore
import org.apache.log4j.Logger

/**
  * Created by rkilgore on 8/18/17.
  */
class StoryContent extends JsonSerializable {
  @JsonIgnore val log: Logger = Logger.getLogger(getClass.getName)
  @JsonIgnore def getSentiment: String = {"neutral"}
  @JsonIgnore override def toString: String = this.toJson
}

//date/revenue pairs named current/previousMetric to conform with downstream expectations
class ClosingComparisonContent(val metric: String, val currentMetric: RevenueOnDate, val previousMetric: RevenueOnDate)
    extends StoryContent {

  override def getSentiment: String = {
    if (this.currentMetric.value > this.previousMetric.value)
      "positive"
    else
      "negative"
  }
}

// {"day":5,"points":[{"date":"2013-03-01","revenue":4404.46},{"date":"2013-03-08","revenue":1553.9900000000002},{"date":"2013-03-15","revenue":0}]}
// day is day of the week (0=Sunday 1=Monday etc..)
class LastKDaysContent(val day: Int, val points: List[RevenueOnDate2])
  extends StoryContent {

  override def getSentiment: String = {
    if (this.points(0).revenue > this.points(1).revenue)
      "negative"
    else
      "positive"
  }
}

//revenue named value to conform with downstream expectations
class RevenueOnDate(val date: String, val value: Double)
class RevenueOnDate2(val date: String, val revenue: Double)

class PeriodBestDayContent(val metric: String, val period: String = "Month", val value: Double, val date: String, val average: Double)
  extends StoryContent {

  @JsonIgnore override def getSentiment: String = "positive"
}

class IntervalSummaryContent(val startDate: String, val endDate: String, val metrics: RevenueAndTransaction)
  extends StoryContent {

  @JsonIgnore override def getSentiment: String = "neutral"
}

class RevenueAndTransaction(val revenue: Double, val transactions: Int)

class UpcomingHolidayContent(val name: String, val date: String) extends StoryContent {
  @JsonIgnore override def getSentiment: String = { "positive" }
}
