FROM hseeberger/scala-sbt
COPY ./StoryGenerator /opt/palantir/StoryGenerator/

# RUN cd /tmp && wget --no-check-certificate https://pki.palantir.com/ca-download/PalantirThirdGenRootCA-selfsign.pem
# RUN keytool -import -trustcacerts -file /tmp/PalantirThirdGenRootCA-selfsign.pem -alias Palantir_ThirdGenRootCA -keystore /docker-java-home/jre/lib/security/cacerts -storepass changeit -noprompt

COPY ./savecerts.sh /opt/palantir
RUN /opt/palantir/savecerts.sh

WORKDIR /opt/palantir/StoryGenerator

RUN sbt compile

CMD sbt "~ ; compile ; test"

