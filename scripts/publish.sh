#!/usr/bin/env bash

PROJECT_NAME="StoryGenerator"
SHORT_HASH=$(git rev-parse --short HEAD)
ALMANAC_BRANCH="$(echo $CIRCLE_BRANCH)"
BUILD_NUMBER=$CIRCLE_BUILD_NUM
TARFILE="$PROJECT_NAME-$BUILD_NUMBER.tar.gz"

DIRNAME=$PROJECT_NAME-$BUILD_NUMBER

echo "Will publish with the following"
echo "branch   : " $ALMANAC_BRANCH
echo "filename : " $TARFILE

echo `pwd`
mkdir ./$DIRNAME/

echo `pwd`
echo `ls $DIRNAME`

mv ./StoryGenerator/target/scala-2.12/StoryGenerator-1.0.jar ./$DIRNAME/
echo `pwd`
echo `ls $DIRNAME`

tar -zcvf $PROJECT_NAME-$BUILD_NUMBER.tar.gz ./$PROJECT_NAME-$BUILD_NUMBER

ART_URL="https://publish.artifactory.palantir.build/artifactory/internal-dist-release/com/palantir/valhalla/$PROJECT_NAME/$BUILD_NUMBER/$TARFILE"

# Artifactory
echo "Publishing to Artifactory: $ART_URL"
curl -v -k -XPUT -L -u $ARTIFACTORY_USERNAME:$ARTIFACTORY_PASSWORD --data-binary @$TARFILE "$ART_URL" --retry 10

### Almanac

json="$(printf '{"product": "%s", "branch": "%s", "revision": "%s", "url": "%s", "metadata" : {"gitrevision" : "%s"}, "tags" : ["automation_verified"]}' $PROJECT_NAME $ALMANAC_BRANCH $BUILD_NUMBER $ART_URL $SHORT_HASH)"

echo "Publishing to Almanac:"
echo "$json"
./scripts/almanac-cli.sh -P -a "${ALMANAC_ACCESS_ID}" -s "${ALMANAC_SECRET_KEY}" -b "${json}" https://almanac.palantir.com/v1/units

echo "Tagging as GA release"
./scripts/almanac-cli.sh -P -a "${ALMANAC_ACCESS_ID}" -s "${ALMANAC_SECRET_KEY}" -b '{"name" : "GA"}' https://almanac.palantir.com/v1/units/${PROJECT_NAME}/${ALMANAC_BRANCH}/${BUILD_NUMBER}/releases

echo "done"
