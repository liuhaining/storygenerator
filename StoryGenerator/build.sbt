name := "StoryGenerator"

version := "1.0"

scalaVersion := "2.12.3"

assemblyJarName in assembly := "StoryGenerator-1.0.jar"
test in assembly := {}

mainClass in (Compile, run) := Some("StoryGenerator.StoryGenerator")

libraryDependencies ++=  Seq(
	"org.scalactic" %% "scalactic" % "3.0.1",
	"org.scalatest" %% "scalatest" % "3.0.1",
	"com.typesafe.akka" % "akka-actor_2.12" % "2.5.4",
	"org.postgresql" % "postgresql" % "42.1.4",
	"com.fasterxml.jackson.module" % "jackson-module-scala_2.12" % "2.9.0",
 	"org.slf4j" % "slf4j-log4j12" % "1.7.12"
	)

unmanagedBase := baseDirectory.value / "lib"