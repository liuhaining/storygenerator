package StoryGenerator.Preconditions

import java.time.LocalDate

import Utils.{Holidays, StoryConfig}

import scala.collection.mutable.ListBuffer

/**
  * Created by rkilgore on 9/19/17.
  */
class UpcomingHolidayPreconditions(initialStores: List[Int], targetDate: LocalDate, val storyType: String = "upcoming-holiday")
  extends AbstractStoryPrecondition(initialStores) {

  // If there is an upcoming holiday, then all stores can reasonably receive a story about it
  if (
    ! (0 to 4)
      .map(d => targetDate.plusDays(d))
      .map(date => Holidays.getDayHolidayOrNull(date) != null)
      .reduce((a, b) => a || b)
  ) {
    stores = ListBuffer.empty[Int]
  }
}
