package StoryGeneratorTest

import java.time.LocalDate
import java.time.temporal.TemporalAdjuster

import org.scalatest.FunSuite

/**
  * Created by rkilgore on 8/30/17.
  */
class StoryGeneratorSmokeTest extends FunSuite {
  test("render a story to postgres") {
    val runDate:LocalDate = LocalDate.now().withDayOfMonth(1)
    System.out.println ("Testing for: " + runDate.toString())
    StoryGenerator.StoryGenerator.main(Array(runDate.toString()))

  }
}
