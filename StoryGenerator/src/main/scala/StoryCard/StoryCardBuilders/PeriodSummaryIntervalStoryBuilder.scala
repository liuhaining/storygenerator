package StoryCard.StoryCardBuilders

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard._

/**
  * Created by rkilgore on 9/6/17.
  */
abstract class PeriodSummaryIntervalStoryBuilder(storiesBackend: ValhallaApi) extends AbstractStoryBuilder(storiesBackend) {

  override def addMetricsAndSentiment(store: Int, date: LocalDate): AbstractStoryBuilder = {
    this.data = getMetricsJson(store, date)
    this.addSentiment()
    this
  }

  def getMetricsJson(store: Int, date: LocalDate): StoryContent = {
    val merchant = storiesBackend.storeIdToMerchant(store)
    val startDate: LocalDate = subtractStoryIntervalFromDate(date)
    val endDate: LocalDate = date
    val revenue = storiesBackend.getSumOfMetricBetween(store, "revenue", startDate, endDate) / 100.0
    val payment_count = storiesBackend.getSumOfMetricBetween(store, "payment_count", startDate, endDate)

    val revenueAndTransaction: RevenueAndTransaction = new StoryCard.RevenueAndTransaction(revenue, payment_count.toInt)
    new IntervalSummaryContent(startDate.toString, endDate.toString, revenueAndTransaction)
  }

  def addSentiment(): AbstractStoryBuilder = {
    this.sentiment = "neutral"
    this
  }

  def subtractStoryIntervalFromDate(date: LocalDate): LocalDate
}