package StoryGenerator.Preconditions

import java.time.LocalDate

/**
  * Created by rkilgore on 9/11/17.
  */
class PeriodSummaryMonthlyStoryPreconditions(initialStores: List[Int], targetDate: LocalDate, val storyType: String = "period-summary-monthly")
  extends AbstractStoryPrecondition(initialStores) {
  if (targetDate.getDayOfMonth != 1) {
    stores.clear()
  }
  else {
    val merchantsWithRevenueOverInterval = storiesBackend.getMerchantsWithRevenueOverInterval(targetDate.minusMonths(1), targetDate)
    stores = stores.filter(storiesBackend.merchantsToStoreIdList(merchantsWithRevenueOverInterval).contains)
  }
}
