package scala.StoryCard.StoryCardBuilders

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard.StoryCardBuilders.{AbstractStoryBuilder, AbstractStoryCardBuilderObject, PeriodSummaryIntervalStoryBuilder}

class PeriodSummaryQuarterlyStoryBuilder(storiesBackend: ValhallaApi) extends PeriodSummaryIntervalStoryBuilder(storiesBackend) {
  override val storyType: String = s"period-summary-quarterly"

  override def subtractStoryIntervalFromDate(date: LocalDate): LocalDate = {
    date.minusMonths(3)
  }
}

object PeriodSummaryQuarterlyStoryBuilder extends AbstractStoryCardBuilderObject {

  override def storyIsValid(storeId: Int, date: LocalDate): Boolean = {
    true
  }

  def getBuilder(storiesBackend: ValhallaApi): AbstractStoryBuilder = {
    new PeriodSummaryQuarterlyStoryBuilder(storiesBackend)
  }
}