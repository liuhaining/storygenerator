package StoryCard.StoryCardBuilders

import java.time.LocalDate
import java.util.concurrent.ConcurrentHashMap

import StoryBackend.ValhallaApi
import StoryCard.UpcomingHolidayContent
import Utils.Holidays

/**
  * Created by rkilgore on 9/19/17.
  */
class UpcomingHolidayStoryBuilder(storiesBackend: ValhallaApi)
  extends AbstractStoryBuilder(storiesBackend) {
  override val storyType: String = "upcoming-holiday"
  datasource = "application"

  override def addMetricsAndSentiment(store: Int, date: LocalDate): AbstractStoryBuilder = {
    this.data = getMetricsJson(date)

    this.addSentiment()
    this
  }

  def getMetricsJson(date: LocalDate) = {
    val holidayName: String = Holidays.getDayHolidayOrNull(date)
    val dateString: String = date.toString
    new UpcomingHolidayContent(holidayName, dateString)
  }

  def addSentiment(): AbstractStoryBuilder = {
    this.sentiment = "positive"
    this
  }
}

object UpcomingHolidayStoryBuilder extends  AbstractStoryCardBuilderObject {
  override def storyIsValid(storeId: Int, date: LocalDate): Boolean = {
    Holidays.getDayHolidayOrNull(date) != null
  }

  override def getBuilder(storiesBackend: ValhallaApi): AbstractStoryBuilder = {
    new UpcomingHolidayStoryBuilder(storiesBackend)
  }
}
