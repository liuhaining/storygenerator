package StoryBuilderTest

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard.StoryCardBuilders.ClosingComparisonStoryBuilder
import org.apache.log4j.Logger
import org.scalatest.FunSuite

/**
  * Created by rkilgore on 8/15/17.
  */
class ClosingComparisonStoryTest extends FunSuite {
//  val log: Logger = Logger.getLogger(getClass.getName)
//  val storiesBackend = new ValhallaApi
//
//  val idTestDefault: String = "IdTestValue"
//  val storyDateTestDefault: String = "StoryDateTestValue"
//  val normalizedTargetDateTestDefault: String = "NormalizedTargetDateTestValue"
//  val generationDateTestDefault: String = "GenerationDateTestValue"
//  val sentimentTestDefault: String = "negative"
//  val isRecalledTestDefault: String = "IsRecalledTestValue"
//  val lastRefreshedTimestampTestDefault: String = "LastRefreshedTimestampTestValue"
//  val storeIdTestDefault: Int = -2
//  val productVersionTestDefault: String = "ProductVersionTestValue"
//  val datasourceTestDefault: String = "DatasourceTestValue"
//
//  val storyTypeClosingComparison: String = "closing-comparison"
//
//  def createCCStory() = {
//    val ccstory = new ClosingComparisonStoryBuilder(storiesBackend)
//      .addId(idTestDefault)
//      .addStoryDate(storyDateTestDefault)
//      .addNormalizedTargetDate(normalizedTargetDateTestDefault)
//      .addGenerationDate(generationDateTestDefault)
//      .addIsRecalled(isRecalledTestDefault)
//      .addLastRefreshedTimestamp(lastRefreshedTimestampTestDefault)
//      .addStoreId(storeIdTestDefault)
//      .addProductVersion(productVersionTestDefault)
//      .addDatasource(datasourceTestDefault)
//        .addMetricsAndSentiment(123, LocalDate.now)
//      .build
//    ccstory
//  }

  test("Values get assigned to the correct variables through the builder") {
//    val ccstory = createCCStory

//    assert(ccstory.id == idTestDefault)
//    assert(ccstory.storyDate == storyDateTestDefault)
//    assert(ccstory.normalizedTargetDate == normalizedTargetDateTestDefault)
//    assert(ccstory.generationDate == generationDateTestDefault)
//    assert(ccstory.sentiment == sentimentTestDefault)
//    assert(ccstory.isRecalled == isRecalledTestDefault)
//    assert(ccstory.lastRefreshedTimestamp == lastRefreshedTimestampTestDefault)
//    assert(ccstory.storeId == storeIdTestDefault)
//    assert(ccstory.productVersion == productVersionTestDefault)
//    assert(ccstory.datasource == datasourceTestDefault)
  }

  test("Just print json and pass") {
//    val storyJson = createCCStory toJson
//    val storyJsonPretty = createCCStory toJsonPretty
//
//    log.info(storyJson)
//    log.info(storyJsonPretty)

  }
}
