package Utils

import java.time.{DayOfWeek, LocalDate}

import scala.collection.mutable.ListBuffer

/**
  * Created by rkilgore on 9/8/17.
  */
object DateUtils {
  def getWeekOfDaysStartingOnDay(day: LocalDate): List[LocalDate] = {
    (0 to 6).map(i => day.plusDays(i)).toList
  }

  def getAllInMonthSameDaysOfWeek(day: LocalDate): List[LocalDate] = {
    val dates: ListBuffer[LocalDate] = ListBuffer.empty[LocalDate]
    var date = day
    val originalMonth = date.getMonth
    while (date.getMonth == originalMonth) {
      dates.append(date)
      date = date.plusWeeks(1)
    }
    dates.toList
  }

  def getListsOfDayOfWeekMembersForMonthIncludingDay(day: LocalDate): List[List[LocalDate]] = {
    getWeekOfDaysStartingOnDay(day.withDayOfMonth(1)).map(getAllInMonthSameDaysOfWeek)
  }

  def getFirstDayOfMonthWithDayOfWeek(startDate: LocalDate, dayOfWeek: DayOfWeek): LocalDate = {
    var date = startDate
    while (date.getDayOfWeek != dayOfWeek) {
      date = date.plusDays(1)
    }
    date
  }
}
