CREATE SCHEMA streaming;

CREATE TYPE streaming.customer_class AS ENUM (
  'all',
  'new',
  'returning',
  'local',
  'nonlocal'
);

CREATE TYPE streaming.time_window AS ENUM (
  'daily',
  'weekly',
  'monthly',
  'yearly',
  'eternal'
);
