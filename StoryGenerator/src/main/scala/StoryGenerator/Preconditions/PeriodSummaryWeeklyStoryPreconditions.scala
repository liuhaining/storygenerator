package StoryGenerator.Preconditions

import java.time.{DayOfWeek, LocalDate}

/**
  * Created by rkilgore on 9/11/17.
  */
class PeriodSummaryWeeklyStoryPreconditions(initialStores: List[Int], targetDate: LocalDate, val storyType: String = "period-summary-weekly")
  extends AbstractStoryPrecondition(initialStores) {
  if (targetDate.getDayOfWeek != DayOfWeek.SUNDAY) {
    stores.clear()
  }
  else {
    val merchantsWithRevenueOverInterval = storiesBackend.getMerchantsWithRevenueOverInterval(targetDate.minusWeeks(1), targetDate)
    stores = stores.filter(storiesBackend.merchantsToStoreIdList(merchantsWithRevenueOverInterval).contains)
  }
}
