package StoryBackend

import java.sql.{Connection, DriverManager, ResultSet}
import java.util.Properties

import Utils.StoryConfig
import org.apache.log4j.Logger
import org.postgresql.Driver

/**
  * Created by rkilgore on 8/14/17.
  */
abstract class PostgresSource {
  val url: String
  val user: String
  val password: String

  val config: StoryConfig = StoryConfig.get
  val log = Logger.getLogger(getClass.getName)
  val driver: Driver = new Driver

  var connection: Connection = null

  def configure() = {
    log.info(s"Creating connection for user $user with url $url")
    connection = DriverManager.getConnection(url, user, password)
  }

  def query(queryString: String): ResultSet = {
    log.debug(s"Execute Query :\t$queryString")
    connection.createStatement().executeQuery(queryString)
  }

  def queryUpdate(queryString: String): Unit = {
    log.debug(s"Execute Update Query :\t$queryString")
    connection.createStatement().executeUpdate(queryString)
  }
}

class StoriesDataSource extends PostgresSource {
  override val url: String =
    s"jdbc:postgresql://${config.storiesUrl}?ssl=${config.storiesSslEnable}"
  override val user: String = config.storiesUser
  override val password: String = config.storiesPassword
  val table: String = config.storiesTable
  configure()

}

class ValhallaWebDataSource extends PostgresSource {
  override val url: String =
    s"jdbc:postgresql://${config.valhallaWebUrl}?ssl=${config.valhallaWebSslEnable}"
  override val user: String = config.valhallaWebUser
  override val password: String = config.valhallaWebPassword
  configure()
}

class StreamingDataSource extends PostgresSource {
  override val url: String =
    s"jdbc:postgresql://${config.streamingUrl}?ssl=${config.streamingSslEnable}"
  override val user: String = config.streamingUser
  override val password: String = config.streamingPassword
  configure()
}
