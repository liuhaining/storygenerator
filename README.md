# StoryGenerator
Story generation for the streaming-go era

See the wiki for documentation - [here](https://github.palantir.build/valhalla/storygenerator/wiki)

# Comments on Development Process
Our development process relies heavily on Docker (preferably Docker-for-Mac). 

Currently, developing this product against Docker relies on a Postgres image loaded with the schema from Streaming-Go.

See here for instructions on setting up your development environment - [link](https://github.palantir.build/valhalla/StoryGenerator/wiki/Setting-Up-Your-Dev-Enviornment)

Updated development environment setup (11/13/2018):
This process will create the needed docker postgres container and populate it with test data.
- > ./docker-setup.sh
- > docker-compose up
- to connect to postgres: psql -h 0.0.0.0 -p 5432 -U postgres
- to clean up if needed: docker-compose rm

# Documentation
To be written and linked here: 
- [x] Getting up and running with StoryGenerator Dev in Docker
- [x] Very basic architecture 
- [ ] Missing pieces, what is left to do
- [x] Scoping discussion, what decisions we've made, why, and what data exists to back them up
