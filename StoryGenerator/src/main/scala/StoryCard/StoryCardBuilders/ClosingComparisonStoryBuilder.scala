package StoryCard.StoryCardBuilders

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard._

/**
  * Created by rkilgore on 8/14/17.
  */

class ClosingComparisonStoryBuilder(storiesBackend: ValhallaApi) extends AbstractStoryBuilder(storiesBackend) {
  override val storyType: String = "closing-comparison"

  override def addMetricsAndSentiment(store: Int, date: LocalDate): AbstractStoryBuilder = {
    this.data = getMetricsJson(store, date)
    this.addSentiment(this.data)
    this
  }

  private def getMetricsJson(store: Int, date: LocalDate): ClosingComparisonContent = {
    val metricType = "revenue"

    val recentRevenue = ClosingComparisonStoryBuilder.getMerchantRevenueOnDay(storiesBackend, store, date)
    val previousRevenue = ClosingComparisonStoryBuilder.getMerchantRevenueOneWeekBefore(storiesBackend, store, date)

    val currentRevenueMetric = new RevenueOnDate(
      date.toString,
      recentRevenue
    )
    val previousRevenueMetric = new RevenueOnDate(
      date.minusDays(7).toString,
      previousRevenue
    )

    new ClosingComparisonContent(metricType, currentRevenueMetric, previousRevenueMetric)
  }

  private def addSentiment(data: StoryContent): AbstractStoryBuilder = {
    this.sentiment = data.getSentiment
    this
  }
}

object ClosingComparisonStoryBuilder extends AbstractStoryCardBuilderObject {
  def storyIsValid(store: Int, date: LocalDate): Boolean = {
    //no specific checks to implement yet
    true
  }

  def getMerchantRevenueOnDay(storiesBackend: ValhallaApi, store: Int, date: LocalDate) = {
    val merchant = storiesBackend.storeIdToMerchant(store)
    storiesBackend.getMerchantRevenueOnDay(merchant.merchant_id, merchant.platform_id, date)
  }

  def getMerchantRevenueOneWeekBefore(storiesBackend: ValhallaApi, store: Int, date: LocalDate) = {
    val merchant = storiesBackend.storeIdToMerchant(store)
    storiesBackend.getMerchantRevenueOneWeekBefore(merchant.merchant_id, merchant.platform_id, date)
  }

  def getBuilder(storiesBackend: ValhallaApi): AbstractStoryBuilder = {
    new ClosingComparisonStoryBuilder(storiesBackend)
  }
}