import Merchant.Merchant
import org.scalatest.FunSuite

/**
  * Created by rkilgore on 9/21/17.
  */
class MerchantTest extends FunSuite {
  test("Merchant from string is agnostic of leading and ending whitespace") {
    val merchants: List[Merchant] =
      List(
        Merchant.fromMidPidString("123456780_1000"),
        Merchant.fromMidPidString(" 123456781_1000"),
        Merchant.fromMidPidString("123456782_1000 "),
        Merchant.fromMidPidString(" 123456783_1000 "),
        Merchant.fromMidPidString("  123456784_1000  ")
      )

    val goldMerchants: List[Merchant] =
      List(
        new Merchant(123456780, 1000),
        new Merchant(123456781, 1000),
        new Merchant(123456782, 1000),
        new Merchant(123456783, 1000),
        new Merchant(123456784, 1000)
      )

    (0 to goldMerchants.size - 1).par
      .foreach(i => assert(merchants(i) == goldMerchants(i)))

  }

  test("Merchant can not be compared with non-matching type") {
    val merchant: Merchant = new Merchant(123456789, 1000)
    val merchantStorePair = (new Merchant(123456789, 1000), 100)
    assert(
      ! merchant.canEqual(merchantStorePair)
    )
  }

  test("Merchants can be compared") {
    val merchantA: Merchant = new Merchant(123456780, 1000)
    val merchantAPrime: Merchant = new Merchant(123456780, 1000)
    val merchantB: Merchant = new Merchant(123456781, 1000)
    val merchantC: Merchant = new Merchant(123456780, 1001)

    assert(merchantA == merchantAPrime)
    assert(merchantA != merchantB)
    assert(merchantA != merchantC)
    assert(merchantB != merchantC)
  }

}
