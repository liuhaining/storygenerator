package StoryCard.StoryCardBuilders

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard._

/**
  * Created by rkilgore on 8/14/17.
  */

class LastKDaysStoryBuilder(storiesBackend: ValhallaApi) extends AbstractStoryBuilder(storiesBackend) {
  override val storyType: String = "last-k-days"

  override def addMetricsAndSentiment(store: Int, date: LocalDate): AbstractStoryBuilder = {
    this.data = getMetricsJson(store, date)
    this.addSentiment(this.data)
    this
  }

  private def getMetricsJson(store: Int, date: LocalDate): LastKDaysContent = {
    val metricType = "revenue"
    val startDate:LocalDate = date.minusDays(1)
    val lastKDaysRevenue = LastKDaysStoryBuilder.getMerchantRevenueLastKDays(storiesBackend, store, startDate)
    val increasing:Boolean = (lastKDaysRevenue(0)._2 < lastKDaysRevenue(1)._2)

    val trendingRevenues:List[Tuple2[LocalDate, Double]] = lastKDaysRevenue.zipWithIndex.flatMap((point)=> {
      if (point._2 == 0 || (increasing && (point._1._2 > lastKDaysRevenue(point._2 - 1)._2)) ||
        (!increasing && (point._1._2 < lastKDaysRevenue(point._2 - 1)._2)))
        List(point._1) else List()
    })

    // remove any dates that don't match n weeks ago (ie..there are gaps in the dates)
    val filteredRevenues:List[Tuple2[LocalDate, Double]] = trendingRevenues.zipWithIndex.flatMap((point)=> {
      if (point._1._1.equals(startDate.minusWeeks(point._2))) List(point._1) else List()
    }
    )

    if (filteredRevenues.size < 4) {
      throw new Exception("not enough data points with revenue")
    }

    // final check of the 5 percent rule:
    if (!((Math.abs(filteredRevenues(0)._2 - filteredRevenues(filteredRevenues.size - 1)._2) / filteredRevenues(filteredRevenues.size - 1)._2) > .05))
      throw new Exception("revenue change does not meet threshold")

    new LastKDaysContent(startDate.getDayOfWeek.getValue, filteredRevenues.reverse.map(point => new RevenueOnDate2(point._1.toString, point._2)))

  }

  private def addSentiment(data: StoryContent): AbstractStoryBuilder = {
    this.sentiment = data.getSentiment
    this
  }
}

object LastKDaysStoryBuilder extends AbstractStoryCardBuilderObject {

  def storyIsValid(store: Int, date: LocalDate): Boolean = {
    //to prevent multiple queries will return all valid here
    //will throw exceptions above if invalid (this serves the same purpose as !valid)

    true
  }

  def getMerchantRevenueLastKDays(storiesBackend: ValhallaApi, store: Int, date: LocalDate) : List[Tuple2[LocalDate, Double]] = {
    storiesBackend.getMerchantRevenueLastKDays(store, date)
  }

  def getBuilder(storiesBackend: ValhallaApi): AbstractStoryBuilder = {
    new LastKDaysStoryBuilder(storiesBackend)
  }
}