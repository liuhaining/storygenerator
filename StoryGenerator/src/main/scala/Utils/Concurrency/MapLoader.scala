package Utils.Concurrency

import java.util.UUID
import java.util.function.BiFunction

import Merchant.Merchant
import StoryBackend.ValhallaApi
import Utils.Concurrency.MapLoader.SingleMapping
import akka.actor.{Actor, Props}
import akka.actor.Actor.Receive
import akka.japi.JavaPartialFunction
import org.apache.log4j.Logger

/**
  * Created by rkilgore on 9/17/17.
  */
class MapLoader extends Actor {
  val log: Logger = Logger.getLogger(getClass.getName)
  var count: Int = 0

  val uuid: UUID = UUID.randomUUID()
  override def receive: Receive = {
    case SingleMapping(unparsedMerchantStorePair) =>
      count += 1
      log.info(s"${uuid.toString} received a job, it has received $count total")
      addMerchantAndStoreToMap(unparsedMerchantStorePair)
  }

  override def postStop(): Unit = {
    log.info(s"$uuid finished loading map, exiting.")
  }

  def addMerchantAndStoreToMap(unparsedMerchantStorePair: Tuple2[String, Int]): Unit = {
    val merchant: Merchant = Merchant.fromMidPidString(unparsedMerchantStorePair._1)
    val store: Int = unparsedMerchantStorePair._2
    val singleStoreList: List[Int] = List[Int](store)

    ValhallaApi.storeToMerchantMap.putIfAbsent(store, merchant)

    val mergeLogic: BiFunction[List[Int], List[Int], List[Int]] = (l1: List[Int], l2: List[Int]) => l1 ++ l2
    ValhallaApi.merchantsToStoresMap.merge(merchant, singleStoreList, mergeLogic)

  }
}

object MapLoader {
  def props: Props = {
    Props(classOf[MapLoader])
  }
  final case class SingleMapping(unparsedMerchantStorePair: Tuple2[String, Int])
}