CREATE SCHEMA sbp_all;

CREATE VIEW sbp_all.metrics AS
  SELECT
    null::SMALLINT                 AS platform_id,
    null::BIGINT                   AS merchant_id,
    null::streaming.time_window    AS time_window,
    null::DATE                     AS date,
    null::streaming.customer_class AS customer_class,
    null::INT                      AS payment_count,
    null::BIGINT                   AS revenue,
    null::INT ARRAY[24]            AS hourly_payment_count,
    null::BIGINT ARRAY[24]         AS hourly_revenue,
    null::INT ARRAY[6]             AS travel_distance_histogram,
    null::INT ARRAY[10]            AS visits_histogram,
    null::JSONB                    AS revenue_by_payment_type,
    null::JSONB                    AS items_sales,
    null::JSON                     AS recent_transactions,
    null::JSONB                    AS customers_by_geohash
  LIMIT 0;

CREATE SCHEMA sbp_registered;

CREATE VIEW sbp_registered.metrics AS
  SELECT
    null::SMALLINT                 AS platform_id,
    null::BIGINT                   AS merchant_id,
    null::streaming.time_window    AS time_window,
    null::DATE                     AS date,
    null::streaming.customer_class AS customer_class,
    null::INT                      AS payment_count,
    null::BIGINT                   AS revenue,
    null::INT ARRAY[24]            AS hourly_payment_count,
    null::BIGINT ARRAY[24]         AS hourly_revenue,
    null::INT ARRAY[6]             AS travel_distance_histogram,
    null::INT ARRAY[10]            AS visits_histogram,
    null::JSONB                    AS revenue_by_payment_type,
    null::JSONB                    AS items_sales,
    null::JSON                     AS recent_transactions,
    null::JSONB                    AS customers_by_geohash
  LIMIT 0;
