#!/bin/bash 

platform_ids=(1000 1000 1000 1000 1001 1001 1001 1001)
merchant_ids=(372472156887 372472156888 372472156889 372472156890 472472156887 472472156888 472472156889 472472156890)
NUMBER_OF_MERCHANTS=8

day_of_week=$(date +%u) # It prints out 1 - 7
# In streaming, first day of week is Sunday

last_n_days() { # Creates array of dates starting with (today-N days) and ending with (today)
  declare -a DAYS_ARRAY
  for i in `seq 1 $1`;
  do
    DAYS_ARRAY[$i]=`date -v -$(($1 - ${i}))d "+%Y-%m-%d"`;
  done;
}



function insert_data {


  echo "insert into sbp_registered.metrics (
  platform_id,
  merchant_id,
  time_window,
  date,
  customer_class,
  payment_count,
  revenue)
values
  ($platform_id, $merchant_id, '$1', '$2', '$3', $4, $5);
  "
}

NUMBER_OF_WEEKS=8
NUMBER_OF_DAYS=$(($NUMBER_OF_WEEKS*7))
IDX=1

# Guarenteed Random Numbers (TM)
RANDOM_NUMBERS=(1 2 3 4 5 6 7 10 20 30 40 50 60 70)
last_n_days $NUMBER_OF_DAYS

add_to_week_sum() {
  WEEK_SUM=$(($WEEK_SUM+$1))
}

increment_or_reset_values_index() { 
#  if [ $IDX -eq 13 ]; then
#    IDX=1
#  else
    IDX=$(($IDX+1))
#  fi
}

populate_db_daily() {
  mid_idx=1
  for mid_idx in `seq 0 $(($NUMBER_OF_MERCHANTS-1))`;
  do
    merchant_id=${merchant_ids[$mid_idx]}
    platform_id=${platform_ids[$mid_idx]}
    echo $merchant_id $platform_id >> datagen.log
    echo "insert into sbp_registered_integration.registered_merchants (
          platform_id,
          merchant_id,
          registration_timestamp)
         values ($platform_id, $merchant_id, current_timestamp) ON CONFLICT DO NOTHING;"

    echo "insert into public.stores (
              store_mid)
             values ('${merchant_id}_${platform_id}');"

    for week in `seq 1 $NUMBER_OF_WEEKS`;
    do
      WEEK_SUM=0
      for day in `seq 1 7`;
      do
        #VALUE=${RANDOM_NUMBERS[$IDX]}
        VALUE=$(($IDX+10))
        DAY_NUM=$((($week-1) * 7 + $day - 1))
        DATE=`date -v -$(($DAY_NUM))d "+%Y-%m-%d"`
        #echo $DATE
        insert_data "daily" $DATE $1 $VALUE $(($VALUE * 10))
        increment_or_reset_values_index
        add_to_week_sum $VALUE
      done
    done
  done

}

#populate_db_daily "new"
populate_db_daily "all" 


# Also these queries are useful but haven't had the time to debug, works fine if you execute them within postgres though
# CREATE TABLE IF NOT EXISTS stories (id int, story_date date, data text, normalized_target_date date, generation_date date, type text, sentiment text, is_recalled text, last_refreshed_timestamp text, store_id text, product_version text, datasource text);
# INSERT INTO stores (store_mid, store_name, base_filters, comp_filters, store_address, backfill_status, store_zipcode, start_of_day_offset, stream_status, comp_filter_generation_method, comp_filter_created_date, store_icon_name, factual_taxonomy_id, price_category, factual_interested_neighborhood, store_id, program_indicator, trial_start_date, store_init_status, creation_ts, program_indicator_enabled_ts, clover_merchant_id, clover_auth_token, backfill_status_last_update_ts, is_clover_enabled, latitude, longitude, location_mode, map_zoom, switch_date, switch_date_override, is_prereg, timezone_name, clover_plan_name, paywall_first_login_ts, paywall_will_upgrade_ts) VALUES ('372472156887_1000', 'REAl STORE 1', '[]', '[]', '100 HAMILTON', 'not_started', 10014, 0, 'synchronized', 'unknown', 1485323309655, 'businessuser', null, null, null, 123, 'f', 1485323309655, 'COMPLETE', 1485323309655, null, null, null, 1485323309655, 'f', 40.0, -100.0, 'synchronize', 12, null, 'f', 't', 'America/New York', null, null, null);
# INSERT INTO stores (store_mid, store_name, base_filters, comp_filters, store_address, backfill_status, store_zipcode, start_of_day_offset, stream_status, comp_filter_generation_method, comp_filter_created_date, store_icon_name, factual_taxonomy_id, price_category, factual_interested_neighborhood, store_id, program_indicator, trial_start_date, store_init_status, creation_ts, program_indicator_enabled_ts, clover_merchant_id, clover_auth_token, backfill_status_last_update_ts, is_clover_enabled, latitude, longitude, location_mode, map_zoom, switch_date, switch_date_override, is_prereg, timezone_name, clover_plan_name, paywall_first_login_ts, paywall_will_upgrade_ts) VALUES ('372472156888_1000', 'REAl STORE 2', '[]', '[]', '100 HAMILTON', 'not_started', 10014, 0, 'synchronized', 'unknown', 1485323309655, 'businessuser', null, null, null, 124, 'f', 1485323309655, 'COMPLETE', 1485323309655, null, null, null, 1485323309655, 'f', 40.0, -100.0, 'synchronize', 12, null, 'f', 't', 'America/New York', null, null, null);
# INSERT INTO stores (store_mid, store_name, base_filters, comp_filters, store_address, backfill_status, store_zipcode, start_of_day_offset, stream_status, comp_filter_generation_method, comp_filter_created_date, store_icon_name, factual_taxonomy_id, price_category, factual_interested_neighborhood, store_id, program_indicator, trial_start_date, store_init_status, creation_ts, program_indicator_enabled_ts, clover_merchant_id, clover_auth_token, backfill_status_last_update_ts, is_clover_enabled, latitude, longitude, location_mode, map_zoom, switch_date, switch_date_override, is_prereg, timezone_name, clover_plan_name, paywall_first_login_ts, paywall_will_upgrade_ts) VALUES ('372472156889_1000', 'REAl STORE 3', '[]', '[]', '100 HAMILTON', 'not_started', 10014, 0, 'synchronized', 'unknown', 1485323309655, 'businessuser', null, null, null, 125, 'f', 1485323309655, 'COMPLETE', 1485323309655, null, null, null, 1485323309655, 'f', 40.0, -100.0, 'synchronize', 12, null, 'f', 't', 'America/New York', null, null, null);
# INSERT INTO stores (store_mid, store_name, base_filters, comp_filters, store_address, backfill_status, store_zipcode, start_of_day_offset, stream_status, comp_filter_generation_method, comp_filter_created_date, store_icon_name, factual_taxonomy_id, price_category, factual_interested_neighborhood, store_id, program_indicator, trial_start_date, store_init_status, creation_ts, program_indicator_enabled_ts, clover_merchant_id, clover_auth_token, backfill_status_last_update_ts, is_clover_enabled, latitude, longitude, location_mode, map_zoom, switch_date, switch_date_override, is_prereg, timezone_name, clover_plan_name, paywall_first_login_ts, paywall_will_upgrade_ts) VALUES ('372472156890_1000', 'REAl STORE 4', '[]', '[]', '100 HAMILTON', 'not_started', 10014, 0, 'synchronized', 'unknown', 1485323309655, 'businessuser', null, null, null, 126, 'f', 1485323309655, 'COMPLETE', 1485323309655, null, null, null, 1485323309655, 'f', 40.0, -100.0, 'synchronize', 12, null, 'f', 't', 'America/New York', null, null, null);
# INSERT INTO stores (store_mid, store_name, base_filters, comp_filters, store_address, backfill_status, store_zipcode, start_of_day_offset, stream_status, comp_filter_generation_method, comp_filter_created_date, store_icon_name, factual_taxonomy_id, price_category, factual_interested_neighborhood, store_id, program_indicator, trial_start_date, store_init_status, creation_ts, program_indicator_enabled_ts, clover_merchant_id, clover_auth_token, backfill_status_last_update_ts, is_clover_enabled, latitude, longitude, location_mode, map_zoom, switch_date, switch_date_override, is_prereg, timezone_name, clover_plan_name, paywall_first_login_ts, paywall_will_upgrade_ts) VALUES ('472472156887_1001', 'NOT A SHELL COMPANY 1', '[]', '[]', '100 HAMILTON', 'not_started', 10014, 0, 'synchronized', 'unknown', 1485323309655, 'businessuser', null, null, null, 321, 'f', 1485323309655, 'COMPLETE', 1485323309655, null, null, null, 1485323309655, 'f', 40.0, -100.0, 'synchronize', 12, null, 'f', 't', 'America/New York', null, null, null);
# INSERT INTO stores (store_mid, store_name, base_filters, comp_filters, store_address, backfill_status, store_zipcode, start_of_day_offset, stream_status, comp_filter_generation_method, comp_filter_created_date, store_icon_name, factual_taxonomy_id, price_category, factual_interested_neighborhood, store_id, program_indicator, trial_start_date, store_init_status, creation_ts, program_indicator_enabled_ts, clover_merchant_id, clover_auth_token, backfill_status_last_update_ts, is_clover_enabled, latitude, longitude, location_mode, map_zoom, switch_date, switch_date_override, is_prereg, timezone_name, clover_plan_name, paywall_first_login_ts, paywall_will_upgrade_ts) VALUES ('472472156888_1001', 'NOT A SHELL COMPANY 2', '[]', '[]', '100 HAMILTON', 'not_started', 10014, 0, 'synchronized', 'unknown', 1485323309655, 'businessuser', null, null, null, 322, 'f', 1485323309655, 'COMPLETE', 1485323309655, null, null, null, 1485323309655, 'f', 40.0, -100.0, 'synchronize', 12, null, 'f', 't', 'America/New York', null, null, null);
# INSERT INTO stores (store_mid, store_name, base_filters, comp_filters, store_address, backfill_status, store_zipcode, start_of_day_offset, stream_status, comp_filter_generation_method, comp_filter_created_date, store_icon_name, factual_taxonomy_id, price_category, factual_interested_neighborhood, store_id, program_indicator, trial_start_date, store_init_status, creation_ts, program_indicator_enabled_ts, clover_merchant_id, clover_auth_token, backfill_status_last_update_ts, is_clover_enabled, latitude, longitude, location_mode, map_zoom, switch_date, switch_date_override, is_prereg, timezone_name, clover_plan_name, paywall_first_login_ts, paywall_will_upgrade_ts) VALUES ('472472156889_1001', 'NOT A SHELL COMPANY 3', '[]', '[]', '100 HAMILTON', 'not_started', 10014, 0, 'synchronized', 'unknown', 1485323309655, 'businessuser', null, null, null, 323, 'f', 1485323309655, 'COMPLETE', 1485323309655, null, null, null, 1485323309655, 'f', 40.0, -100.0, 'synchronize', 12, null, 'f', 't', 'America/New York', null, null, null);
# INSERT INTO stores (store_mid, store_name, base_filters, comp_filters, store_address, backfill_status, store_zipcode, start_of_day_offset, stream_status, comp_filter_generation_method, comp_filter_created_date, store_icon_name, factual_taxonomy_id, price_category, factual_interested_neighborhood, store_id, program_indicator, trial_start_date, store_init_status, creation_ts, program_indicator_enabled_ts, clover_merchant_id, clover_auth_token, backfill_status_last_update_ts, is_clover_enabled, latitude, longitude, location_mode, map_zoom, switch_date, switch_date_override, is_prereg, timezone_name, clover_plan_name, paywall_first_login_ts, paywall_will_upgrade_ts) VALUES ('472472156889_1001', 'NOT A SHELL COMPANY 3', '[]', '[]', '100 HAMILTON', 'not_started', 10014, 0, 'synchronized', 'unknown', 1485323309655, 'businessuser', null, null, null, 324, 'f', 1485323309655, 'COMPLETE', 1485323309655, null, null, null, 1485323309655, 'f', 40.0, -100.0, 'synchronize', 12, null, 'f', 't', 'America/New York', null, null, null);

# Change the date on this one to be today, useful for period-best-day testing
# UPDATE metrics.metrics SET revenue=1000 WHERE date=DATE('2017-09-06') AND platform_id=1001;

