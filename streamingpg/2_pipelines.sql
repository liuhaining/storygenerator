-- Schema sbp_all exists only for integration testing,
-- it does not exist in the production environment.
CREATE SCHEMA sbp_all_integration;

-- Table sbp_all_integration.metrics is used as an underlying relation for the sbp_all.metrics view.
CREATE TABLE sbp_all_integration.metrics (
  platform_id               SMALLINT                 NOT NULL,
  merchant_id               BIGINT                   NOT NULL,
  time_window               streaming.time_window    NOT NULL,
  date                      DATE                     NOT NULL,
  customer_class            streaming.customer_class NOT NULL,
  payment_count             INT,
  revenue                   BIGINT,
  hourly_payment_count      INT ARRAY[24],
  hourly_revenue            BIGINT ARRAY[24],
  travel_distance_histogram INT ARRAY[6],
  visits_histogram          INT ARRAY[10],
  revenue_by_payment_type   JSONB,
  items_sales               JSONB,
  recent_transactions       JSON,
  customers_by_geohash      JSONB
);

CREATE OR REPLACE VIEW sbp_all.metrics AS
  SELECT
    platform_id,
    merchant_id,
    time_window,
    date,
    customer_class,
    payment_count,
    revenue,
    hourly_payment_count,
    hourly_revenue,
    travel_distance_histogram,
    visits_histogram,
    revenue_by_payment_type,
    items_sales,
    recent_transactions,
    customers_by_geohash
  FROM sbp_all_integration.metrics;

-- Schema sbp_registered exists only for integration testing,
-- it does not exist in the production environment.
CREATE SCHEMA sbp_registered_integration;

-- Table sbp_registered_integration.metrics is used as an underlying relation for the sbp_registered.metrics view.
CREATE TABLE sbp_registered_integration.metrics (
  platform_id               SMALLINT                 NOT NULL,
  merchant_id               BIGINT                   NOT NULL,
  time_window               streaming.time_window    NOT NULL,
  date                      DATE                     NOT NULL,
  customer_class            streaming.customer_class NOT NULL,
  payment_count             INT,
  revenue                   BIGINT,
  hourly_payment_count      INT ARRAY[24],
  hourly_revenue            BIGINT ARRAY[24],
  travel_distance_histogram INT ARRAY[6],
  visits_histogram          INT ARRAY[10],
  revenue_by_payment_type   JSONB,
  items_sales               JSONB,
  recent_transactions       JSON,
  customers_by_geohash      JSONB
);

CREATE OR REPLACE VIEW sbp_registered.metrics AS
  SELECT
    platform_id,
    merchant_id,
    time_window,
    date,
    customer_class,
    payment_count,
    revenue,
    hourly_payment_count,
    hourly_revenue,
    travel_distance_histogram,
    visits_histogram,
    revenue_by_payment_type,
    items_sales,
    recent_transactions,
    customers_by_geohash
  FROM sbp_registered_integration.metrics;



CREATE TABLE sbp_registered_integration.registered_merchants (
				serial_id              SERIAL      PRIMARY KEY,
				platform_id            INT         NOT NULL,
				merchant_id            BIGINT      NOT NULL,
				registration_timestamp TIMESTAMP   NOT NULL,
				CONSTRAINT unique_platform_id_merchant_id UNIQUE (platform_id, merchant_id)
			);

CREATE OR REPLACE VIEW sbp_registered.registered_merchants AS
             SELECT
				serial_id,
				platform_id,
				merchant_id,
				registration_timestamp
			FROM sbp_registered_integration.registered_merchants;