package StoryGenerator.Preconditions

import java.time.LocalDate

/**
  * Created by rkilgore on 9/11/17.
  */
class PeriodSummaryQuarterlyStoryPreconditions(initialStores: List[Int], targetDate: LocalDate, val storyType: String = "period-summary-quarterly")
extends AbstractStoryPrecondition(initialStores) {
  if ((targetDate.getMonthValue % 3) != 1
    || targetDate.getDayOfMonth != 1) {
    stores.clear()
  }
  else {
    val merchantsWithRevenueOverInterval = storiesBackend.getMerchantsWithRevenueOverInterval(targetDate.minusMonths(3), targetDate)
    stores = stores.filter(storiesBackend.merchantsToStoreIdList(merchantsWithRevenueOverInterval).contains)

  }
}
