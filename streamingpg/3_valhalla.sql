--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.9
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: -
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_confirmation; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.account_confirmation (
    user_id integer,
    link_hash character varying,
    valid_until timestamp without time zone,
    time_confirmed timestamp without time zone
);


--
-- Name: accounts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.accounts (
    user_id integer,
    key character varying,
    data character varying
);


--
-- Name: app_events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.app_events (
    user_id integer,
    username character varying,
    user_ip_addr character varying,
    session_id character varying,
    component character varying,
    action character varying,
    useragent_source character varying,
    device character varying,
    browser character varying,
    browser_version character varying,
    details character varying,
    user_ts timestamp without time zone,
    server_ts timestamp without time zone
);


--
-- Name: app_languages; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.app_languages (
    user_id integer,
    app_language character varying
);


--
-- Name: auth; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.auth (
    user_id integer,
    password_hash character varying,
    salt character varying
);


--
-- Name: bookmarks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.bookmarks (
    id integer NOT NULL,
    store_id integer,
    user_id integer,
    type character varying,
    key character varying,
    title character varying,
    note character varying,
    metadata character varying,
    deleted boolean DEFAULT false,
    added_date date,
    deleted_date date,
    note_date date,
    added_datetime timestamp without time zone,
    note_datetime timestamp without time zone
);


--
-- Name: bookmarks_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.bookmarks_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: bookmarks_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.bookmarks_id_seq OWNED BY public.bookmarks.id;


--
-- Name: clover_app_install_statuses; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.clover_app_install_statuses (
    clover_merchant_id character varying NOT NULL,
    is_installed boolean NOT NULL,
    modified_at timestamp without time zone,
    created_at timestamp without time zone NOT NULL
);


--
-- Name: clover_data_pipeline_status; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.clover_data_pipeline_status (
    store_id integer,
    pipeline_status character varying,
    user_id bigint,
    "timestamp" bigint,
    host_name character varying,
    association_id character varying
);


--
-- Name: clover_features; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.clover_features (
    store_id integer,
    new_feature character varying
);


--
-- Name: clover_smart_auth; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.clover_smart_auth (
    log_id integer NOT NULL,
    user_id integer,
    store_id bigint,
    time_logged timestamp without time zone,
    clover_data json,
    auth_data json,
    request_data json,
    error_data json
);


--
-- Name: clover_smart_auth_credentials; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.clover_smart_auth_credentials (
    clover_merchant_id character varying,
    clover_access_token character varying,
    last_updated timestamp without time zone
);


--
-- Name: clover_smart_auth_log_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.clover_smart_auth_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: clover_smart_auth_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.clover_smart_auth_log_id_seq OWNED BY public.clover_smart_auth.log_id;


--
-- Name: cohorts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.cohorts (
    store_id integer,
    name character varying,
    generation_method character varying,
    latitude numeric,
    longitude numeric,
    radius numeric,
    merchant_ids character varying,
    update_timestamp timestamp without time zone
);


--
-- Name: email_events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_events (
    sg_event_id character varying,
    sg_message_id character varying,
    event character varying,
    email character varying,
    sbp_email_type character varying,
    "timestamp" character varying,
    useragent character varying,
    url character varying,
    full_event text
);


--
-- Name: email_registration; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_registration (
    email character varying,
    hash character varying,
    created timestamp without time zone
);


--
-- Name: email_subscriptions; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.email_subscriptions (
    email_type character varying,
    email_address character varying,
    mid character varying,
    subscribed boolean,
    "timestamp" timestamp without time zone
);


--
-- Name: feedbacks; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.feedbacks (
    user_id integer,
    date_submitted timestamp without time zone,
    app_version character varying,
    url character varying,
    feedback character varying
);


--
-- Name: stores; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stores (
    store_id integer NOT NULL,
    store_mid character varying,
    store_name character varying,
    store_address character varying,
    store_zipcode character varying,
    timezone_name character varying,
    latitude numeric,
    longitude numeric,
    location_mode character varying,
    map_zoom integer,
    base_filters character varying,
    comp_filters character varying,
    comp_filter_generation_method character varying,
    comp_filter_created_date bigint,
    backfill_status character varying,
    backfill_status_last_update_ts bigint,
    start_of_day_offset integer,
    store_icon_name character varying,
    factual_taxonomy_id integer,
    price_category integer,
    factual_interested_neighborhood character varying,
    stream_status character varying,
    program_indicator boolean,
    program_indicator_enabled_ts bigint,
    trial_start_date bigint,
    store_init_status character varying,
    creation_ts bigint,
    clover_auth_token character varying,
    clover_merchant_id character varying,
    clover_plan_name character varying,
    is_clover_enabled boolean,
    switch_date bigint,
    switch_date_override boolean,
    is_prereg boolean,
    paywall_first_login_ts bigint,
    paywall_will_upgrade_ts bigint
);


--
-- Name: user_ids; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_ids (
    user_id integer NOT NULL,
    username character varying,
    user_type character varying NOT NULL
);


--
-- Name: user_store_mappings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_store_mappings (
    store_id bigint,
    user_id integer,
    user_level character varying
);


--
-- Name: full_user_store_mappings_vw; Type: VIEW; Schema: public; Owner: -
--

CREATE VIEW public.full_user_store_mappings_vw AS
 SELECT a.user_id,
    a.username,
    a.user_type,
    b.user_level,
    c.store_id,
    c.store_mid,
    c.is_prereg,
    c.clover_merchant_id
   FROM ((public.user_ids a
     JOIN public.user_store_mappings b ON ((a.user_id = b.user_id)))
     JOIN public.stores c ON ((b.store_id = c.store_id)));


--
-- Name: goals_global; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.goals_global (
    store_id character varying,
    type character varying,
    json_data character varying
);


--
-- Name: job_leases; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.job_leases (
    is_lease boolean,
    expiration timestamp without time zone,
    id character varying(40),
    namespace character varying(40)
);


--
-- Name: key_values; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.key_values (
    key text NOT NULL,
    value text
);


--
-- Name: locations; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.locations (
    id integer,
    data character varying,
    keywords character varying,
    latitude real,
    longitude real
);


--
-- Name: mobile_devices; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.mobile_devices (
    id integer NOT NULL,
    user_id integer,
    uuid character varying,
    ios_device_token character varying,
    ios_token_active boolean DEFAULT false,
    ios_token_marked_inactive_at timestamp without time zone,
    ios_token_marked_active_at timestamp without time zone,
    created_at timestamp without time zone,
    last_seen_at timestamp without time zone
);


--
-- Name: mobile_devices_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.mobile_devices_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: mobile_devices_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.mobile_devices_id_seq OWNED BY public.mobile_devices.id;


--
-- Name: notification_events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notification_events (
    notification_id integer,
    notification_type character varying,
    store_id integer,
    user_id integer,
    event_type character varying,
    event_timestamp timestamp without time zone
);


--
-- Name: notification_prefs; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notification_prefs (
    user_id integer,
    notification_type character varying,
    is_enabled boolean
);


--
-- Name: notifications; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.notifications (
    notification_id integer NOT NULL,
    notification_type character varying,
    user_id integer,
    store_id integer,
    data character varying,
    generation_time timestamp without time zone
);


--
-- Name: notifications_notification_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.notifications_notification_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: notifications_notification_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.notifications_notification_id_seq OWNED BY public.notifications.notification_id;


--
-- Name: objects; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.objects (
    hash character varying,
    type character varying,
    value character varying
);


--
-- Name: sbp_auth; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbp_auth (
    user_id integer,
    password_hash character varying,
    salt character varying,
    password_expiration bigint,
    last_expiration_warning bigint,
    first_time_user bigint,
    failed_attempts integer,
    locked_out_until bigint
);


--
-- Name: sbp_demo_auth; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbp_demo_auth (
    mid character varying,
    email_address character varying,
    demo_token character varying,
    time_generated character varying,
    time_visited character varying,
    expired boolean
);


--
-- Name: sbp_opened_email_links; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbp_opened_email_links (
    email_id integer,
    redirect_url character varying,
    date_opened timestamp without time zone
);


--
-- Name: sbp_password_log; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbp_password_log (
    user_id integer,
    password_hash character varying,
    salt character varying,
    last_used bigint
);


--
-- Name: sbp_password_reset; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sbp_password_reset (
    user_id integer,
    link_hash character varying,
    valid_until bigint
);


--
-- Name: secrets; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.secrets (
    id character varying,
    key character varying,
    secret character varying,
    valid_until bigint
);


--
-- Name: sent_emails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.sent_emails (
    app_version character varying,
    date_sent timestamp without time zone,
    delivered boolean,
    email_address character varying,
    email_subject character varying,
    email_from character varying,
    email_recipient_overrides character varying,
    email_subject_override character varying,
    email_from_override character varying,
    email_id integer NOT NULL,
    email_secret character varying,
    email_type character varying,
    email_tracking_override character varying,
    is_internal_email boolean,
    used_litmus boolean,
    mid character varying
);


--
-- Name: sent_emails_email_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.sent_emails_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: sent_emails_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.sent_emails_email_id_seq OWNED BY public.sent_emails.email_id;


--
-- Name: social_media; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.social_media (
    user_id integer,
    key character varying,
    data character varying
);


--
-- Name: stored_filters; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stored_filters (
    key character varying,
    serialized_filter character varying
);


--
-- Name: stores_store_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stores_store_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stores_store_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stores_store_id_seq OWNED BY public.stores.store_id;


--
-- Name: stories; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.stories (
    id integer NOT NULL,
    story_date date,
    store_id integer,
    data character varying,
    type character varying,
    sentiment character varying,
    generation_date timestamp without time zone,
    normalized_target_date date,
    is_recalled boolean,
    last_refreshed_timestamp timestamp without time zone,
    product_version character varying DEFAULT 'unknown'::character varying,
    datasource character varying
);


--
-- Name: stories_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.stories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: stories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.stories_id_seq OWNED BY public.stories.id;


--
-- Name: story_events; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.story_events (
    story_id integer,
    user_id integer,
    event_type character varying,
    event_date timestamp without time zone,
    metadata json DEFAULT '{}'::json
);


--
-- Name: system_properties; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.system_properties (
    key character varying NOT NULL,
    value character varying
);


--
-- Name: unsent_emails; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.unsent_emails (
    app_version character varying,
    date_sent timestamp without time zone,
    delivered boolean,
    reason_for_failure character varying,
    email_address character varying,
    email_subject character varying,
    email_from character varying,
    email_recipient_overrides character varying,
    email_subject_override character varying,
    email_from_override character varying,
    email_id integer NOT NULL,
    email_secret character varying,
    email_type character varying,
    email_tracking_override character varying,
    is_internal_email boolean,
    used_litmus boolean,
    mid character varying
);


--
-- Name: unsent_emails_email_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.unsent_emails_email_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: unsent_emails_email_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.unsent_emails_email_id_seq OWNED BY public.unsent_emails.email_id;


--
-- Name: user_cohort_mappings; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_cohort_mappings (
    cohort_id integer,
    user_id integer
);


--
-- Name: user_cohorts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_cohorts (
    cohort_id integer NOT NULL,
    name character varying,
    generation_method character varying,
    latitude numeric,
    longitude numeric,
    radius numeric,
    category_ids numeric[],
    merchant_ids character varying[],
    create_timestamp timestamp without time zone,
    update_timestamp timestamp without time zone,
    metadata json
);


--
-- Name: user_cohorts_cohort_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_cohorts_cohort_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_cohorts_cohort_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_cohorts_cohort_id_seq OWNED BY public.user_cohorts.cohort_id;


--
-- Name: user_ids_user_id_seq; Type: SEQUENCE; Schema: public; Owner: -
--

CREATE SEQUENCE public.user_ids_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


--
-- Name: user_ids_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: -
--

ALTER SEQUENCE public.user_ids_user_id_seq OWNED BY public.user_ids.user_id;


--
-- Name: user_indexed_cohorts; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_indexed_cohorts (
    user_id integer,
    indexed_cohort_id character varying,
    create_timestamp timestamp without time zone,
    metadata json
);


--
-- Name: user_roles; Type: TABLE; Schema: public; Owner: -
--

CREATE TABLE public.user_roles (
    user_id integer,
    role_name character varying
);


--
-- Name: bookmarks id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bookmarks ALTER COLUMN id SET DEFAULT nextval('public.bookmarks_id_seq'::regclass);


--
-- Name: clover_smart_auth log_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clover_smart_auth ALTER COLUMN log_id SET DEFAULT nextval('public.clover_smart_auth_log_id_seq'::regclass);


--
-- Name: mobile_devices id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mobile_devices ALTER COLUMN id SET DEFAULT nextval('public.mobile_devices_id_seq'::regclass);


--
-- Name: notifications notification_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications ALTER COLUMN notification_id SET DEFAULT nextval('public.notifications_notification_id_seq'::regclass);


--
-- Name: sent_emails email_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sent_emails ALTER COLUMN email_id SET DEFAULT nextval('public.sent_emails_email_id_seq'::regclass);


--
-- Name: stores store_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stores ALTER COLUMN store_id SET DEFAULT nextval('public.stores_store_id_seq'::regclass);


--
-- Name: stories id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stories ALTER COLUMN id SET DEFAULT nextval('public.stories_id_seq'::regclass);


--
-- Name: unsent_emails email_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unsent_emails ALTER COLUMN email_id SET DEFAULT nextval('public.unsent_emails_email_id_seq'::regclass);


--
-- Name: user_cohorts cohort_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_cohorts ALTER COLUMN cohort_id SET DEFAULT nextval('public.user_cohorts_cohort_id_seq'::regclass);


--
-- Name: user_ids user_id; Type: DEFAULT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_ids ALTER COLUMN user_id SET DEFAULT nextval('public.user_ids_user_id_seq'::regclass);


--
-- Name: auth authuniqueuserid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.auth
    ADD CONSTRAINT authuniqueuserid UNIQUE (user_id);


--
-- Name: bookmarks bookmarks_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.bookmarks
    ADD CONSTRAINT bookmarks_pkey PRIMARY KEY (id);


--
-- Name: clover_smart_auth clover_smart_auth_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clover_smart_auth
    ADD CONSTRAINT clover_smart_auth_pkey PRIMARY KEY (log_id);


--
-- Name: account_confirmation emailconfirmationuniquehash; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_confirmation
    ADD CONSTRAINT emailconfirmationuniquehash UNIQUE (link_hash);


--
-- Name: account_confirmation emailconfirmationuniqueuserid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.account_confirmation
    ADD CONSTRAINT emailconfirmationuniqueuserid UNIQUE (user_id);


--
-- Name: email_registration emailregistrationuniqueemail; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_registration
    ADD CONSTRAINT emailregistrationuniqueemail UNIQUE (email);


--
-- Name: email_registration emailregistrationuniquehash; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.email_registration
    ADD CONSTRAINT emailregistrationuniquehash UNIQUE (hash);


--
-- Name: key_values key_values_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.key_values
    ADD CONSTRAINT key_values_pkey PRIMARY KEY (key);


--
-- Name: locations locations_id_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.locations
    ADD CONSTRAINT locations_id_key UNIQUE (id);


--
-- Name: mobile_devices mobile_devices_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mobile_devices
    ADD CONSTRAINT mobile_devices_pkey PRIMARY KEY (id);


--
-- Name: objects objects_hash_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.objects
    ADD CONSTRAINT objects_hash_key UNIQUE (hash);


--
-- Name: sbp_auth sbpauthuniqueuserid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbp_auth
    ADD CONSTRAINT sbpauthuniqueuserid UNIQUE (user_id);


--
-- Name: sbp_password_reset sbppasswordresetuniquehash; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbp_password_reset
    ADD CONSTRAINT sbppasswordresetuniquehash UNIQUE (link_hash);


--
-- Name: sbp_password_reset sbppasswordresetuniqueuserid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbp_password_reset
    ADD CONSTRAINT sbppasswordresetuniqueuserid UNIQUE (user_id);


--
-- Name: cohorts sbpuniquecohort; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.cohorts
    ADD CONSTRAINT sbpuniquecohort UNIQUE (store_id);


--
-- Name: user_indexed_cohorts sbpuniqueindexedcohort; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_indexed_cohorts
    ADD CONSTRAINT sbpuniqueindexedcohort UNIQUE (user_id, indexed_cohort_id);


--
-- Name: social_media socialuniqueuserkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.social_media
    ADD CONSTRAINT socialuniqueuserkey UNIQUE (user_id, key);


--
-- Name: stories stories_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stories
    ADD CONSTRAINT stories_pkey PRIMARY KEY (id);


--
-- Name: system_properties system_properties_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.system_properties
    ADD CONSTRAINT system_properties_pkey PRIMARY KEY (key);


--
-- Name: sent_emails unique_email_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sent_emails
    ADD CONSTRAINT unique_email_id PRIMARY KEY (email_id);


--
-- Name: stored_filters unique_key; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stored_filters
    ADD CONSTRAINT unique_key UNIQUE (key);


--
-- Name: clover_data_pipeline_status unique_store_id; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clover_data_pipeline_status
    ADD CONSTRAINT unique_store_id UNIQUE (store_id);


--
-- Name: clover_smart_auth_credentials uniqueclovermerchantid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.clover_smart_auth_credentials
    ADD CONSTRAINT uniqueclovermerchantid UNIQUE (clover_merchant_id);


--
-- Name: sbp_demo_auth uniquedemotoken; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.sbp_demo_auth
    ADD CONSTRAINT uniquedemotoken UNIQUE (demo_token);


--
-- Name: job_leases uniquejobleaseid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.job_leases
    ADD CONSTRAINT uniquejobleaseid UNIQUE (id);


--
-- Name: notifications uniquenotificationid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT uniquenotificationid PRIMARY KEY (notification_id);


--
-- Name: notification_prefs uniqueprefid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.notification_prefs
    ADD CONSTRAINT uniqueprefid UNIQUE (user_id, notification_type);


--
-- Name: secrets uniquesecretid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.secrets
    ADD CONSTRAINT uniquesecretid UNIQUE (id);


--
-- Name: stores uniquestoreid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.stores
    ADD CONSTRAINT uniquestoreid PRIMARY KEY (store_id);


--
-- Name: user_cohort_mappings uniqueusercohortmapping; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_cohort_mappings
    ADD CONSTRAINT uniqueusercohortmapping UNIQUE (cohort_id, user_id);


--
-- Name: app_languages uniqueuserid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.app_languages
    ADD CONSTRAINT uniqueuserid UNIQUE (user_id);


--
-- Name: accounts uniqueuserkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT uniqueuserkey UNIQUE (user_id, key);


--
-- Name: user_ids uniqueusernames; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_ids
    ADD CONSTRAINT uniqueusernames UNIQUE (username);


--
-- Name: user_store_mappings uniqueuserstoremapping; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_store_mappings
    ADD CONSTRAINT uniqueuserstoremapping UNIQUE (store_id, user_id);


--
-- Name: mobile_devices uniqueuuid; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.mobile_devices
    ADD CONSTRAINT uniqueuuid UNIQUE (uuid);


--
-- Name: unsent_emails unsent_emails_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.unsent_emails
    ADD CONSTRAINT unsent_emails_pkey PRIMARY KEY (email_id);


--
-- Name: user_cohorts user_cohorts_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_cohorts
    ADD CONSTRAINT user_cohorts_pkey PRIMARY KEY (cohort_id);


--
-- Name: user_ids user_ids_pkey; Type: CONSTRAINT; Schema: public; Owner: -
--

ALTER TABLE ONLY public.user_ids
    ADD CONSTRAINT user_ids_pkey PRIMARY KEY (user_id);


--
-- Name: accountusers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX accountusers ON public.accounts USING btree (user_id, key);


--
-- Name: appeventscomponentindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appeventscomponentindex ON public.app_events USING btree (component);


--
-- Name: appeventstimestampindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appeventstimestampindex ON public.app_events USING btree (server_ts);


--
-- Name: appeventsuseridindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX appeventsuseridindex ON public.app_events USING btree (user_id);


--
-- Name: authusers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX authusers ON public.auth USING btree (user_id);


--
-- Name: event; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX event ON public.email_events USING btree (event);


--
-- Name: goalsglobalstoreid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX goalsglobalstoreid ON public.goals_global USING btree (store_id);


--
-- Name: identuserids; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX identuserids ON public.user_ids USING btree (user_id);


--
-- Name: identusernames; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX identusernames ON public.user_ids USING btree (username);


--
-- Name: identusertypes; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX identusertypes ON public.user_ids USING btree (user_type);


--
-- Name: index_by_demo_token; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_by_demo_token ON public.sbp_demo_auth USING btree (demo_token);


--
-- Name: index_by_email_address; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX index_by_email_address ON public.sbp_demo_auth USING btree (email_address);


--
-- Name: mobile_devices_index_by_ios_token; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX mobile_devices_index_by_ios_token ON public.mobile_devices USING btree (ios_device_token);


--
-- Name: mobile_devices_index_by_user_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX mobile_devices_index_by_user_id ON public.mobile_devices USING btree (user_id);


--
-- Name: mobile_devices_index_by_uuid; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX mobile_devices_index_by_uuid ON public.mobile_devices USING btree (uuid);


--
-- Name: objectshashindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX objectshashindex ON public.objects USING btree (hash, type);


--
-- Name: opened_links_index_by_email_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX opened_links_index_by_email_id ON public.sbp_opened_email_links USING btree (email_id);


--
-- Name: opened_links_index_by_redirect_url; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX opened_links_index_by_redirect_url ON public.sbp_opened_email_links USING btree (redirect_url);


--
-- Name: rolesindexbyuser; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX rolesindexbyuser ON public.user_roles USING btree (user_id);


--
-- Name: sbpauthusers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sbpauthusers ON public.sbp_auth USING btree (user_id);


--
-- Name: sbppasswordlogusers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sbppasswordlogusers ON public.sbp_password_log USING btree (user_id);


--
-- Name: secretsbykey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX secretsbykey ON public.secrets USING btree (key);


--
-- Name: secretsbytime; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX secretsbytime ON public.secrets USING btree (valid_until);


--
-- Name: sent_emails_index_by_email_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sent_emails_index_by_email_id ON public.sent_emails USING btree (email_id);


--
-- Name: sent_emails_index_by_email_secret; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sent_emails_index_by_email_secret ON public.sent_emails USING btree (email_secret);


--
-- Name: sentemailaddressindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX sentemailaddressindex ON public.sent_emails USING btree (email_address);


--
-- Name: socialusers; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX socialusers ON public.social_media USING btree (user_id, key);


--
-- Name: storebookmarks; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX storebookmarks ON public.bookmarks USING btree (store_id, type);


--
-- Name: storedfiltersindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX storedfiltersindex ON public.stored_filters USING btree (key);


--
-- Name: storeids; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX storeids ON public.stores USING btree (store_id);


--
-- Name: storiessearchindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX storiessearchindex ON public.stories USING btree (normalized_target_date, type, store_id);


--
-- Name: storiesstoreidindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX storiesstoreidindex ON public.stories USING btree (store_id);


--
-- Name: storyeventsstoryidindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX storyeventsstoryidindex ON public.story_events USING btree (story_id);


--
-- Name: unsent_emails_index_by_email_id; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX unsent_emails_index_by_email_id ON public.unsent_emails USING btree (email_id);


--
-- Name: unsent_emails_index_by_email_secret; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX unsent_emails_index_by_email_secret ON public.unsent_emails USING btree (email_secret);


--
-- Name: unsentemailaddressindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX unsentemailaddressindex ON public.unsent_emails USING btree (email_address);


--
-- Name: usercohortmappingscohortindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usercohortmappingscohortindex ON public.user_cohort_mappings USING btree (cohort_id);


--
-- Name: usercohortmappingsuserindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX usercohortmappingsuserindex ON public.user_cohort_mappings USING btree (user_id);


--
-- Name: userids; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userids ON public.app_languages USING btree (user_id);


--
-- Name: userstorebookmarkkey; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userstorebookmarkkey ON public.bookmarks USING btree (user_id, store_id, type, key, deleted);


--
-- Name: userstorebookmarks; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userstorebookmarks ON public.bookmarks USING btree (user_id, store_id, type);


--
-- Name: userstoremappingsstoreindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userstoremappingsstoreindex ON public.user_store_mappings USING btree (store_id);


--
-- Name: userstoremappingsuserindex; Type: INDEX; Schema: public; Owner: -
--

CREATE INDEX userstoremappingsuserindex ON public.user_store_mappings USING btree (user_id);


--
-- PostgreSQL database dump complete
--

