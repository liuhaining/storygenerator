package StoryCard.StoryCardBuilders

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard._
import Utils.StoryConfig
import org.apache.log4j.Logger

import scala.StoryCard.StoryCardBuilders.{PeriodSummaryMonthlyStoryBuilder, PeriodSummaryQuarterlyStoryBuilder, PeriodSummaryWeeklyStoryBuilder}

/**
  * Created by rkilgore on 8/17/17.
  */

abstract class AbstractStoryBuilder(val storiesBackend: ValhallaApi) {
  val log: Logger = Logger.getLogger(getClass.getName)
  val config: StoryConfig = StoryConfig.get

  var id: Int = _
  var storyDate: LocalDate= _
  var normalizedTargetDate: LocalDate= _
  var generationDate: LocalDate= _
  var sentiment: String = ""
  var isRecalled: Boolean = false
  var lastRefreshedTimestamp: LocalDate = _
  var storeId: Int = -1
  var productVersion: String = config.version
  var datasource: String = config.streamingTable
  val storyType: String = ""
  var data: StoryContent = _

  def addMetricsAndSentiment(store: Int, date: LocalDate): AbstractStoryBuilder

  def addId(id: Int): AbstractStoryBuilder = {
    this.id = id
    this
  }

  def addStoryDate(storyDate: LocalDate): AbstractStoryBuilder = {
    this.storyDate = storyDate
    this
  }

  def addNormalizedTargetDate(normalizedTargetDate: LocalDate): AbstractStoryBuilder = {
    this.normalizedTargetDate = normalizedTargetDate
    this
  }

  def addGenerationDate(generationDate: LocalDate): AbstractStoryBuilder = {
    this.generationDate = generationDate
    this
  }

  def addIsRecalled(isRecalled: Boolean): AbstractStoryBuilder = {
    this.isRecalled = isRecalled
    this
  }

  def addLastRefreshedTimestamp(lastRefreshedTimestamp: LocalDate): AbstractStoryBuilder = {
    this.lastRefreshedTimestamp = lastRefreshedTimestamp
    this
  }

  def addStoreId(storeId: Int): AbstractStoryBuilder = {
    this.storeId = storeId
    this
  }

  def addProductVersion(productVersion: String): AbstractStoryBuilder = {
    this.productVersion = productVersion
    this
  }

  def addDatasource(datasource: String): AbstractStoryBuilder = {
    this.datasource = datasource
    this
  }

  def build: Story = new Story(this)

}

object StoryCardBuilders {
  def getObjectForType(storyType: String): AbstractStoryCardBuilderObject = {
    storyType match {
      case "closing-comparison" => ClosingComparisonStoryBuilder
      case "period-best-day" => PeriodBestDayStoryBuilder
      case "period-summary-weekly" => PeriodSummaryWeeklyStoryBuilder
      case "period-summary-monthly" => PeriodSummaryMonthlyStoryBuilder
      case "period-summary-quarterly" => PeriodSummaryQuarterlyStoryBuilder
      case "upcoming-holiday" => UpcomingHolidayStoryBuilder
      case "last-k-days" => LastKDaysStoryBuilder
      case _ => throw new IllegalArgumentException("Invalid story type")
    }
  }
}

trait AbstractStoryCardBuilderObject {
  def storyIsValid(storeId: Int, date: LocalDate): Boolean

  def getBuilder(storiesBackend: ValhallaApi): AbstractStoryBuilder
}