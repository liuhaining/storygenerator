#!/usr/bin/env bash

usage () {
    cat >&2 <<EOM
    Usage: `basename $0` (-G | -P | -U | -D) [-i] -a <access_id> -s <secret_key> [-b <body>] almanac-url-endpoint
    -G  Send a GET request to almanac-url-endpoint
    -P  Send a POST request to almanac-url-endpoint
    -U  Send a PUT request to almanac-url-endpoint
    -D  Send a DELETE request to almanac-url-endpoint
    -i  Do not validate SSL certificates.  Can be useful for testing.  DO NOT USE IN PRODUCTION
EOM
}

body=""

while getopts ':PGUDia:s:b:' opt
do
    case $opt in
        a   ) access=$OPTARG ;;
        s   ) secret=$OPTARG ;;
        b   ) body=$OPTARG ;;
        i   ) insecure='-k' ;;
        P   ) if [[ -z "$method" ]]; then method='-X POST'; else echo 'Can only specify one of -P -G -U -D' >&2 && exit 1; fi;;
        G   ) if [[ -z "$method" ]]; then method='-G'; else echo 'Can only specify one of -P -G -U -D' >&2 && exit 1; fi;;
        D   ) if [[ -z "$method" ]]; then method='-X DELETE'; else echo 'Can only specify one of -P -G -U -D' >&2 && exit 1; fi;;
        U   ) if [[ -z "$method" ]]; then method='-X PUT'; else echo 'Can only specify one of -P -G -U -D' >&2 && exit 1; fi;;
        \?  ) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
        :   ) echo "Missing argument for -$OPTARG" >&2; exit 1 ;;
    esac
done

shift $(($OPTIND - 1))

endpoint=$1



if [[ -z "$method" ]] || [[ -z "$endpoint" ]] || [[ -z "$access" ]] || [[ -z "$secret" ]]; then
    usage
    exit 1
fi


timestamp=$(date +%s)

hash=$(echo -n "${endpoint}${timestamp}${body}" | openssl sha1 -hmac "$secret" -binary | base64 )

if [[ -n $body ]]; then
    curl -s $insecure $method -H "Content-Type: application/json" -H "X-authorization: ${access}:${hash}" -H "X-timestamp: ${timestamp}" -d "${body}" $endpoint
else
    curl -s $insecure $method -H "Content-Type: application/json" -H "X-authorization: ${access}:${hash}" -H "X-timestamp: ${timestamp}" $endpoint
fi
