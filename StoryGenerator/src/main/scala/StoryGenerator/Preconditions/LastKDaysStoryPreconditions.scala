package StoryGenerator.Preconditions

import java.time.LocalDate

/**
  * Created by rkilgore on 9/11/17.
  */
class LastKDaysStoryPreconditions(initialStores: List[Int], targetDate: LocalDate, val storyType: String = "last-k-days")
  extends AbstractStoryPrecondition(initialStores) {
  val merchantsWithTrendingLastKDays = storiesBackend.getMerchantsWithRevenueTrendingOverLastKDays(targetDate.minusDays(1))
  stores = stores.filter(storiesBackend.merchantsToStoreIdList(merchantsWithTrendingLastKDays).contains)
}
