package StoryCard.StoryCardBuilders

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard.PeriodBestDayContent
import Utils.DateUtils

/**
  * Created by rkilgore on 9/5/17.
  */

/** The precondition for period best day makes it such that this class will only be invoked if the current day is
  * the best one within the given period. Therefore, once we are in this builder we no longer have to check whether
  * the most recent day has maximum revenue.
  */
class PeriodBestDayStoryBuilder(storiesBackend: ValhallaApi) extends AbstractStoryBuilder(storiesBackend) {
  override val storyType: String = "period-best-day"

  override def addMetricsAndSentiment(store: Int, date: LocalDate): AbstractStoryBuilder = {
    this.data = getMetricsJson(store, date)
    this.addSentiment()
    this
  }

  def getMetricsJson(store: Int, date: LocalDate) = {
    val merchant = storiesBackend.storeIdToMerchant(store)
    val dateToRevenue: Tuple2[LocalDate, Double] = storiesBackend
      .getMerchantRevenueMaxForPeriodBetween(
        store, date.minusMonths(1).withDayOfMonth(1), date)
      .head

    val dates = DateUtils.getAllInMonthSameDaysOfWeek(dateToRevenue._1)
    val averageRevenue: Double = storiesBackend.getMerchantRevenueAverageForDates(store, dates)
    new PeriodBestDayContent("revenue", "month", dateToRevenue._2, dateToRevenue._1.toString, averageRevenue)
  }

  def addSentiment(): AbstractStoryBuilder = {
    this.sentiment = "positive"
    this
  }
}

object PeriodBestDayStoryBuilder extends AbstractStoryCardBuilderObject {
  def storyIsValid(store: Int, date: LocalDate): Boolean = {
    //no specific checks to implement yet
    true
  }

  def getBuilder(storiesBackend: ValhallaApi): AbstractStoryBuilder = {
    new PeriodBestDayStoryBuilder(storiesBackend)
  }
}