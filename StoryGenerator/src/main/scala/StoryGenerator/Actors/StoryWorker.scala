package StoryGenerator.Actors

import java.time.{LocalDate, LocalTime}

import StoryBackend.ValhallaApi
import StoryCard.StoryCardBuilders.{AbstractStoryCardBuilderObject, StoryCardBuilders}
import StoryGenerator.Actors.StoryWriter.RenderedStory
import akka.actor.{Actor, ActorRef, PoisonPill, Props}
import org.apache.log4j.Logger

/**
  * Created by rkilgore on 8/22/17.
  */
class StoryWorker(storyWriter: ActorRef) extends Actor {
  import StoryWorker._
  val storiesBackend: ValhallaApi = new ValhallaApi

  val log: Logger = Logger.getLogger(getClass.getName)
  val defaultValue = "???"
  val uuid: String = java.util.UUID.randomUUID.toString
  log.debug(s"StoryWorker coming online with id $uuid")

  override def preStart(): Unit = {
    super.preStart()
  }

  override def postStop(): Unit = {
    log.debug(s"Worker unregistering from writer and exiting, id: $uuid")
    storyWriter ! WriterRegistrationMessage.UNREGISTER
  }

  def receive = {
    case StorySpec(store, storyType, id, targetDate) =>
      val storyCardBuilderObject = StoryCardBuilders.getObjectForType(storyType)
      if (storyCardBuilderObject.storyIsValid(store, targetDate)) {
        try {val story = buildStory(storyCardBuilderObject, store, storyType, id, targetDate)
          storyWriter ! RenderedStory(story.toSqlValues)
        } catch {
          case e: Exception => log.error(s"Story of type $storyType for store of id $store is not valid due to: ${e.getMessage}")
        }
      }
      else {
        log.info(s"Story of type $storyType for store of id $store is not valid, passing")
      }

    case catchall =>
      log.info(s"Unsupported storyType: $catchall")
  }

  def buildStory(storyBuilderType: AbstractStoryCardBuilderObject, store: Int, storyType: String, id: Int, targetDate: LocalDate): StoryCard.Story = {
    val today = LocalDate.now
    storyBuilderType.getBuilder(storiesBackend)
      .addId(id)
      .addStoryDate(today)
      .addNormalizedTargetDate(targetDate)
      .addGenerationDate(today)
      .addIsRecalled(false)
      .addLastRefreshedTimestamp(today)
      .addStoreId(store)
      .addProductVersion("2.0.0")
      .addDatasource("clover") // placeholder, has to be either clover, terminal or application. No idea which this should be
      .addMetricsAndSentiment(store, targetDate)
      .build
  }
}

object StoryWorker {
  def props(writer: ActorRef): Props = {
    Props(classOf[StoryWorker], writer)
  }
  final case class StorySpec(store: Int, storyType: String, id: Int, targetDate: LocalDate)
}

object WriterRegistrationMessage extends Enumeration {
  type WriterRegistrationMessage = Value
  val REGISTER, UNREGISTER = Value
}
