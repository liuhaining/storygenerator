package Merchant

/**
  * Created by rkilgore on 8/22/17.
  */
class Merchant(val merchant_id: Long, val platform_id: Int) {
  override def toString: String = toMidPidStringPair

  def toMidPidStringPair = s"${merchant_id}_${platform_id}"

  def canEqual(a: Any) = a.isInstanceOf[Merchant]
  override def equals(that: Any): Boolean = {
    that match {
      case that: Merchant => that.canEqual(this) && this.hashCode == that.hashCode
      case _ => false
    }
  }

  override def hashCode: Int = {
    toString.hashCode
  }
}

object Merchant {
  def fromMidPidString(midPidString: String) = {
    val midPidSplit = midPidString.split("_")
    new Merchant(midPidSplit(0).trim.toLong, midPidSplit(1).trim.toInt)
  }
}