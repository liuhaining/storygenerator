package Utils

import java.io.File
import java.time.LocalDate

import com.typesafe.config.{Config, ConfigFactory}
import org.apache.log4j.Logger

import scala.collection.JavaConverters._

/**
  * Created by rkilgore on 9/11/17.
  */
class StoryConfig {
  val log = Logger.getLogger(getClass.getName)
  var configPath: String = _
  try {
    configPath = sys.env.get("SCALA_CONFIG_PATH").get
  } catch {
    case e: Exception =>
      configPath = "./"
  }

  if ( !configPath.endsWith("/") ) configPath += "/"

  val config: Config = ConfigFactory.parseFile(new File(configPath + "application.conf"))
  log.info(s"Config path is $configPath")
  log.info(s"Config: ${config.toString}")

  val version: String = config.getString("version")

  val workerPoolSize: Int = config.getInt("workerPoolSize")
  val writerQueueSize: Int = config.getInt("writerQueueSize")
  val enabledStories: List[String] = config.getStringList("enabledStories").asScala.toList
  var targetDateOverride: String = null
  var targetDate: LocalDate = null
  try {
    targetDateOverride = config.getString("targetDateOverride")
    targetDate = LocalDate.parse(targetDateOverride)
  } catch {
    case e: Exception =>
      log.info("No target date override set by config")
  }




  val valhallaWebUrl: String = config.getString("valhallaWeb.url")
  val valhallaWebUser: String = config.getString("valhallaWeb.user")
  val valhallaWebPassword: String = config.getString("valhallaWeb.password")
  val valhallaWebSslEnable: String = config.getString("valhallaWeb.sslEnable")

  val streamingUrl: String = config.getString("streaming.url")
  val streamingUser: String = config.getString("streaming.user")
  val streamingPassword: String = config.getString("streaming.password")
  val streamingSslEnable: String = config.getString("streaming.sslEnable")
  val streamingTable: String = config.getString("streaming.table")

  val storiesUrl: String = config.getString("stories.url")
  val storiesUser: String = config.getString("stories.user")
  val storiesPassword: String = config.getString("stories.password")
  val storiesSslEnable: String = config.getString("stories.sslEnable")
  val storiesTable: String = config.getString("stories.table")

}

object StoryConfig {
  private var config: StoryConfig = _
  def get = {
    if ( config == null ) {
      config = new StoryConfig
    }
    config
  }
}
