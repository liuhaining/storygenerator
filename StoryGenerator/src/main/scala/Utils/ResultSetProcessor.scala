package Utils

import java.sql.ResultSet
import java.time.LocalDate

import Merchant.Merchant

import scala.collection.mutable.ListBuffer

/**
  * Created by rkilgore on 8/23/17.
  */
class ResultSetProcessor {

  def toIntListFromColumn(resultSet: ResultSet, columnName: String): List[Int] = {
    val values = ListBuffer[Int]()
    while (resultSet.next()) {
      values.append(resultSet.getInt(columnName))
    }
    values.toList
  }

  def toLongListFromColumn(resultSet: ResultSet, columnName: String): List[Long] = {
    val values = ListBuffer[Long]()
    while (resultSet.next()) {
      values.append(resultSet.getLong(columnName))
    }
    values.toList
  }

  def toMerchantListFromStoreTable(resultSet: ResultSet): List[Merchant] = {
    val merchants = ListBuffer[Merchant]()
    while (resultSet.next()) {
      merchants.append(Merchant.fromMidPidString(resultSet.getString("store_mid")))
    }
    merchants.toList
  }

  def toMerchantListFromMetricsTable(resultSet: ResultSet): List[Merchant] = {
    val merchants = ListBuffer[Merchant]()
    while (resultSet.next()) {
      merchants.append(new Merchant(resultSet.getString("merchant_id").trim.toLong, resultSet.getString("platform_id").trim.toInt))
    }
    merchants.toList
  }

  def toDateRevenuePairs(resultSet: ResultSet): List[Tuple2[LocalDate, Double]] = {
    val datesToRevenues: ListBuffer[Tuple2[LocalDate, Double]] = new ListBuffer[Tuple2[LocalDate, Double]]
    while (resultSet.next()) {
      datesToRevenues.append((resultSet.getDate("date").toLocalDate, resultSet.getDouble("revenue") / 100))
    }
    datesToRevenues.toList
  }

  def toDoubleListFromId(resultSet: ResultSet, columnName: String): List[Double] = {
    val values = ListBuffer[Double]()
    while (resultSet.next()) {
      values.append(resultSet.getDouble(columnName) / 100)
    }
    values.toList
  }

  def toStringIntPairsWithColumnNames(resultSet: ResultSet, stringColumnName: String, intColumnName: String): List[Tuple2[String, Int]] = {
    val values = ListBuffer[Tuple2[String, Int]]()
    while (resultSet.next()) {
      values.append( (resultSet.getString(stringColumnName), resultSet.getInt(intColumnName)) )
    }
    values.toList
  }
}
