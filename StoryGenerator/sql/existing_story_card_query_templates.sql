--enabledStories = ["period-best-day","closing-comparison","period-summary-weekly","period-summary-monthly","period-summary-quarterly", "upcoming-holiday", "last-k-days"]

-- from ValhallaApi


-- getMerchantsWithNonZeroRevenueVaryingSinceLastWeek
SELECT a.merchant_id, a.platform_id, a.revenue, b.revenue 
    FROM ( SELECT merchant_id, platform_id, revenue, date 
            FROM $metricsTable WHERE date='$oneWeekAgo' AND time_window='daily' AND customer_class='all' ) AS a 
        INNER JOIN (  
            SELECT merchant_id, platform_id, revenue, date 
                FROM $metricsTable WHERE date='$today' AND time_window='daily' AND customer_class='all' ) AS b 
                ON a.merchant_id=b.merchant_id 
                AND a.platform_id=b.platform_id AND a.revenue > 0 AND b.revenue > 0 
                AND a.revenue != b.revenue ORDER BY a.merchant_id, a.platform_id;  
  
-- same as below getMerchantRevenueLastKDays (storeId:Int, startDate: LocalDate): List[Tuple2[LocalDate, Double]] = {
        select date, revenue FROM ${metricsTable} where time_window='daily' AND customer_class='all'   
          AND extract(dow from date) = ${dow} and   
          date >= '${endDate.toString}' and date <= '${startDate.toString}' and revenue > 0   
          AND merchant_id=${merchant.merchant_id} AND   
          platform_id=${merchant.platform_id}   
          order by date desc;   

-- getMerchantsWithRevenueOverInterval(startDate: LocalDate, endDate: LocalDate) = {
select merchant_id, platform_id, revenue 
    FROM  (select SUM(revenue) revenue, merchant_id, platform_id FROM  $metricsTable 
       WHERE  
       date>='${startDate.toString}' AND  
       date<'${endDate.toString}' AND  
      time_window='daily' AND  
      customer_class='all' group by merchant_id, platform_id) AS a 
      where revenue > 0; 

-- last-k-days getMerchantsWithRevenueTrendingOverLastKDays(startDate: LocalDate) = {
select distinct merchant_id, platform_id FROM  
         (select *, sum(trend) over(partition by merchant_id, platform_id order by date desc) as streak,  
         (abs(cast(revenue as decimal)-cast(reference as decimal)) / cast(reference as decimal)) as change  
         FROM (select merchant_id, platform_id, revenue, date, prev, reference, row,  
            case when (prev < revenue) then 1 when (prev > revenue) then -1 else 0 end as trend  
         FROM(select merchant_id, platform_id, revenue, date, lag(revenue)  
            over w as prev, first_value(revenue) over w as reference, row_number() over w as row  
         FROM ${metricsTable} where time_window='daily' AND customer_class='all' and  
         extract(dow FROM date) = ${dow} and date >= '${endDate.toString}' and date <= '${startDate.toString}' and revenue > 0  
         window w as (partition by merchant_id, platform_id order by date desc)) as v1) as v2) as v3  
         where row >= 4 and abs (streak) = (row - 1) and  
         (abs(cast(revenue as decimal)-cast(reference as decimal)) / cast(reference as decimal)) > .05; 
         
--   def getMerchantRevenueMaxForPeriodBetween(storeId: Int, windowStart: LocalDate, windowEnd: LocalDate): List[Tuple2[LocalDate, Double]] = {
SELECT date, revenue FROM $metricsTable WHERE     
          revenue=
          (SELECT MAX(revenue) FROM $metricsTable WHERE
              customer_class='all' AND
              time_window='daily' AND
              DATE(date) >= DATE('$windowStart') AND
              DATE(date) <= DATE('$windowEnd') AND
              merchant_id=${merchant.merchant_id} AND
              platform_id=${merchant.platform_id});