package Utils

import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.{JsonSerializer, ObjectMapper, SerializerProvider}
import com.fasterxml.jackson.databind.module.SimpleModule
import com.fasterxml.jackson.module.scala.DefaultScalaModule

/**
  * Created by rkilgore on 8/18/17.
  */
trait JsonSerializable {
  def toJson: String = {
    val mapper = new ObjectMapper()
    val module = new SimpleModule()
    module.addSerializer(classOf[Double], new MyDoubleSerializer())
    mapper.registerModule(DefaultScalaModule)
    mapper.registerModule(module)
    val writer = mapper.writer
    val json = writer.writeValueAsString(this)
    json
  }

  def toJsonPretty: String = {
    val mapper = new ObjectMapper()
    mapper.registerModule(DefaultScalaModule)
    val writer = mapper.writerWithDefaultPrettyPrinter
    val json = writer.writeValueAsString(this)
    json
  }

  case class MyDoubleSerializer() extends JsonSerializer[Double] {
    override def serialize(t: Double,
                           jsonGenerator: JsonGenerator,
                           serializerProvider: SerializerProvider) {
      val d = BigDecimal(t)
      jsonGenerator.writeNumber(d.bigDecimal.toPlainString)
    }
  }
}

