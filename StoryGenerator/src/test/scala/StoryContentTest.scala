import StoryCard._
import org.scalatest.FunSuite

/**
  * Created by rkilgore on 9/26/17.
  */
class StoryContentTest extends FunSuite {

  def fixture = {
    new {
      val closingcomparison_currentMetricHigh = new RevenueOnDate("2017-01-31", 102.00)
      val closingcomparison_currentMetricLow = new RevenueOnDate("2017-01-31", 100.00)
      val closingcomparison_previousMetric = new RevenueOnDate("2017-01-24", 101.00)
      val intervalsummary_revenueAndTransaction = new RevenueAndTransaction(100.0, 5)
      val date_jan1_string = "2017-01-01"
      val date_jan24_string = "2017-01-24"
      val date_jan31_string = "2017-01-31"
    }
  }

  test("closing comparison correctly generates JSON") {
    val f = fixture

    val cccHigh: ClosingComparisonContent =
      new ClosingComparisonContent("revenue", f.closingcomparison_currentMetricHigh, f.closingcomparison_previousMetric)
    val cccLow: ClosingComparisonContent =
      new ClosingComparisonContent("revenue", f.closingcomparison_currentMetricLow, f.closingcomparison_previousMetric)

    assert(
      cccHigh.toString == s"""{"metric":"revenue","currentMetric":{"date":"${f.date_jan31_string}","value":102.0},"previousMetric":{"date":"${f.date_jan24_string}","value":101.0}}"""
    )
    assert(
      cccLow.toString == s"""{"metric":"revenue","currentMetric":{"date":"${f.date_jan31_string}","value":100.0},"previousMetric":{"date":"${f.date_jan24_string}","value":101.0}}"""
    )
  }

  test("period best day correctly generates JSON") {
    val f = fixture

    val pbd: PeriodBestDayContent =
      new PeriodBestDayContent("revenue", "Month", 100.0, f.date_jan1_string, 90.0)
    assert(pbd.toString == """{"metric":"revenue","period":"Month","value":100.0,"date":"2017-01-01","average":90.0}""")
  }

  test("interval summary content correctly generates JSON") {
    val f = fixture

    val isc: IntervalSummaryContent =
      new IntervalSummaryContent(f.date_jan1_string, f.date_jan31_string, f.intervalsummary_revenueAndTransaction)
    assert(
      isc.toString == s"""{"startDate":"${f.date_jan1_string}","endDate":"${f.date_jan31_string}","metrics":{"revenue":100.0,"transactions":5}}"""
    )
  }

  test("upcoming holiday correctly generates JSON") {
    val f = fixture

    val uhc: UpcomingHolidayContent =
      new UpcomingHolidayContent("holiday name", f.date_jan1_string)
    assert(
      uhc.toString == s"""{"name":"holiday name","date":"${f.date_jan1_string}"}"""
    )
  }

}
