package StoryBackend

import java.sql.ResultSet
import java.time.LocalDate
import java.util.concurrent.ConcurrentHashMap

import Merchant.Merchant
import Utils.Concurrency.MapLoader
import Utils.Concurrency.MapLoader.SingleMapping
import Utils.{ResultSetProcessor, StoryConfig}
import akka.actor.{ActorRef, ActorSystem, PoisonPill}
import akka.pattern.gracefulStop
import akka.routing.{Broadcast, RoundRobinPool}
import org.apache.log4j.Logger

import scala.collection.immutable.HashSet
import scala.concurrent.Await
import scala.concurrent.duration.FiniteDuration

/**
  * Created by rkilgore on 8/24/17.
  */
class ValhallaApi {
  val log: Logger = Logger.getLogger(getClass.getName)
  val config: StoryConfig = StoryConfig.get
  private val resultSetProcessor = new ResultSetProcessor

  private var storiesSource: PostgresSource = null
  private var valhallaWebSource: PostgresSource = null
  private var streamingSource: PostgresSource = null

  private var metricsTable: String = config.streamingTable

  def dataSource(name: String) = {
    name match {
      case "stories" =>
        if (storiesSource == null) {storiesSource = new StoriesDataSource}; storiesSource
      case "valhallaWeb" =>
        if (valhallaWebSource == null) {valhallaWebSource= new ValhallaWebDataSource}; valhallaWebSource
      case "streaming" =>
        if (streamingSource == null) {streamingSource = new StreamingDataSource}; streamingSource
    }
  }

  def merchantsToStoreIdList(merchants: List[Merchant]): List[Int] = {
    log.info(s"Using merchant list to look up stores, merchant list has ${merchants.size} members")
    merchants.filter(allMerchantsToStorePairs.containsKey).flatMap(allMerchantsToStorePairs.get)
  }

  def getMerchantRevenueOnDay(merchant_id: Long, platform_id: Int, date: LocalDate): Double = {
    val queryString =

      s"SELECT revenue FROM $metricsTable WHERE " +
        s"date = DATE('${date.toString}') AND " +
        s"merchant_id = $merchant_id AND " +
        s"platform_id = $platform_id AND " +
        s"customer_class='all';"

    val result: ResultSet = dataSource("streaming").query(queryString)
    var revenue: Double = 0
    while (result.next()) {
      revenue = result.getInt("revenue").toDouble / 100
    }
    revenue
  }

  def getMerchantRevenueOneWeekBefore(merchant_id: Long, platform_id: Int, date: LocalDate): Double = {
    getMerchantRevenueOnDay(
      merchant_id,
      platform_id,
      date.minusDays(7))
  }

  def getMerchants = {
    val resultSet: ResultSet =
      dataSource("streaming").query(s"SELECT platform_id, merchant_id FROM sbp_registered.registered_merchants;")
    val merchants: List[Merchant] = resultSetProcessor.toMerchantListFromMetricsTable(resultSet)
    log.info(s"From sbp_registered.registered_merchants we got number of merchants: ${merchants.size}")
    merchants
  }

  def getMostRecentStoryId = {
    val resultSet = dataSource("stories").query("SELECT DISTINCT MAX(id) id FROM stories;")
    resultSetProcessor.toIntListFromColumn(resultSet, "id")(0)
  }

  def getStoresWithStoriesGeneratedOnTargetDate(storyType: String) = {
    val resultSet: ResultSet = dataSource("stories").query(

      s"SELECT DISTINCT * FROM (SELECT MAX(generation_date) AS max_date, store_id FROM stories WHERE type='$storyType' GROUP BY store_id) AS recent_stories WHERE recent_stories.max_date = DATE('${config.targetDate.toString}');"
    )
    resultSetProcessor.toIntListFromColumn(resultSet, "store_id")
  }

  def getMerchantRevenueMaxForPeriodBetween(storeId: Int, windowStart: LocalDate, windowEnd: LocalDate): List[Tuple2[LocalDate, Double]] = {
    val merchant = storeIdToMerchant(storeId)
    val resultSet = dataSource("streaming").query(s"SELECT date, revenue FROM $metricsTable WHERE " +
      s"revenue=(SELECT MAX(revenue) FROM $metricsTable WHERE " +
      s"customer_class='all' AND " +
      s"time_window='daily' AND " +
      s"DATE(date) >= DATE('$windowStart') AND " +
      s"DATE(date) <= DATE('$windowEnd') AND " +
      s"merchant_id=${merchant.merchant_id} AND " +
      s"platform_id=${merchant.platform_id});")
    resultSetProcessor.toDateRevenuePairs(resultSet)
  }

  def getMerchantsWithNonZeroRevenueVaryingSinceLastWeek() = {
    val today = config.targetDate.toString
    val oneWeekAgo = config.targetDate.minusDays(7)

    val resultSet = dataSource("streaming").query(s"SELECT a.merchant_id, a.platform_id, a.revenue, b.revenue FROM ( SELECT merchant_id, platform_id, revenue, date FROM $metricsTable WHERE date='$oneWeekAgo' AND time_window='daily' AND customer_class='all' ) AS a INNER JOIN ( " +
      s"SELECT merchant_id, platform_id, revenue, date FROM $metricsTable WHERE date='$today' AND time_window='daily' AND customer_class='all' ) AS b ON a.merchant_id=b.merchant_id AND a.platform_id=b.platform_id AND a.revenue > 0 AND b.revenue > 0 AND a.revenue != b.revenue ORDER BY a.merchant_id, a.platform_id; ")
    resultSetProcessor.toMerchantListFromMetricsTable(resultSet)
  }

  def getMerchantsWithRevenueOverInterval(startDate: LocalDate, endDate: LocalDate) = {
    val resultSet: ResultSet = dataSource("streaming").query(
      "select merchant_id, platform_id, revenue from " +
      "(select SUM(revenue) revenue, merchant_id, platform_id from " +
      s"$metricsTable where " +
      s"date>='${startDate.toString}' AND " +
      s"date<'${endDate.toString}' AND " +
      "time_window='daily' AND " +
      "customer_class='all' group by merchant_id, platform_id) AS A where revenue > 0;")
    resultSetProcessor.toMerchantListFromMetricsTable(resultSet)

  }

  def getMerchantsWithRevenueTrendingOverLastKDays(startDate: LocalDate) = {
    val endDate:LocalDate = startDate.minusWeeks(5)
    val dow:Int = startDate.getDayOfWeek.getValue

    val resultSet: ResultSet = dataSource ("streaming").query(
      "select distinct merchant_id, platform_id from " +
        "(select *, sum(trend) over(partition by merchant_id, platform_id order by date desc) as streak, " +
        "(abs(cast(revenue as decimal)-cast(reference as decimal)) / cast(reference as decimal)) as change " +
        "from(select merchant_id, platform_id, revenue, date, prev, reference, row, " +
        "case when (prev < revenue) then 1 when (prev > revenue) then -1 else 0 end as trend " +
        "from(select merchant_id, platform_id, revenue, date, lag(revenue) " +
        "over w as prev, first_value(revenue) over w as reference, row_number() over w as row " +
        s"from ${metricsTable} where time_window='daily' AND customer_class='all' and " +
        s"extract(dow from date) = ${dow} and date >= '${endDate.toString}' and date <= '${startDate.toString}' and revenue > 0 " +
        "window w as (partition by merchant_id, platform_id order by date desc)) as v1) as v2) as v3 " +
        "where row >= 4 and abs (streak) = (row - 1) and " +
        "(abs(cast(revenue as decimal)-cast(reference as decimal)) / cast(reference as decimal)) > .05;")

    resultSetProcessor.toMerchantListFromMetricsTable(resultSet)
  }

  def getMerchantRevenueLastKDays (storeId:Int, startDate: LocalDate): List[Tuple2[LocalDate, Double]] = {
    val merchant = storeIdToMerchant(storeId)
    val endDate:LocalDate = startDate.minusWeeks(5)
    val dow:Int = startDate.getDayOfWeek.getValue

    val resultSet: ResultSet = dataSource ("streaming").query(
      s"select date, revenue from ${metricsTable} where time_window='daily' AND customer_class='all' " +
        s"AND extract(dow from date) = ${dow} and " +
        s"date >= '${endDate.toString}' and date <= '${startDate.toString}' and revenue > 0 " +
        s"AND merchant_id=${merchant.merchant_id} AND " +
        s"platform_id=${merchant.platform_id} " +
        s"order by date desc;")

    System.out.println (storeId + " Getting last k days: " + startDate + ":" + endDate + ":" + dow + ":" + resultSet)

    resultSetProcessor.toDateRevenuePairs(resultSet)
  }

  def getSumOfMetricBetween(store: Int, metric: String, startDate: LocalDate, endDate: LocalDate): Long = {
    val merchant: Merchant = storeIdToMerchant(store)
    val resultSet: ResultSet = dataSource("streaming")
      .query(s"select SUM($metric) $metric from $metricsTable where " +
        s"customer_class='all' AND " +
        s"time_window='daily' AND " +
        s"merchant_id=${merchant.merchant_id} AND " +
        s"platform_id=${merchant.platform_id} AND " +
        s"date>='$startDate' AND " +
        s"date<'$endDate';")
    resultSetProcessor.toLongListFromColumn(resultSet, s"$metric").head
  }

  def getMerchantRevenueAverageForDates(storeId: Int, dates: List[LocalDate]): Double = {
    val merchant: Merchant = storeIdToMerchant(storeId)
    val datesString = "'" + dates.map(x => "'" + x.toString + "'" ).mkString(", ").drop(1)
    val query: String = s"SELECT AVG(revenue) revenue FROM $metricsTable WHERE " +
      s"customer_class='all' AND merchant_id=${merchant.merchant_id} AND platform_id=${merchant.platform_id} AND " +
      s"date IN ( ${datesString.toString} );"
    val resultSet: ResultSet = dataSource("streaming").query(query)
    resultSetProcessor.toDoubleListFromId(resultSet, "revenue").head
  }

  def storeIdToMerchant(store: Int) = {
    ValhallaApi.storeToMerchantMap.get(store)
  }

  def addMerchantAndStoreToMap(unparsedMerchantStorePair: Tuple2[String, Int]): Unit = {
    val merchant: Merchant = Merchant.fromMidPidString(unparsedMerchantStorePair._1)
    if (ValhallaApi.merchantsToStoresMap.contains(merchant)) {
      ValhallaApi.merchantsToStoresMap.put(merchant,
        unparsedMerchantStorePair._2 :: ValhallaApi.merchantsToStoresMap.get(merchant)
      )
    } else {
      ValhallaApi.merchantsToStoresMap.put(merchant, List(unparsedMerchantStorePair._2))
    }
  }

  def allMerchantsToStorePairs: ConcurrentHashMap[Merchant, List[Int]] = {
    if (ValhallaApi.merchantsToStoresMap.isEmpty) {
      val system: ActorSystem = ActorSystem("MappingSystem")
      val router: ActorRef = system.actorOf(MapLoader.props.withRouter(RoundRobinPool(1000)))

      val resultSet = dataSource("valhallaWeb").query("SELECT store_id, store_mid from stores;")
      val stringIntPairs: List[(String, Int)] = resultSetProcessor.toStringIntPairsWithColumnNames(resultSet, "store_mid", "store_id")
      log.info(s"Initializing master merchant to stores pair map for ${stringIntPairs.size} merchants")
      stringIntPairs
        .foreach(
        m => {
          router ! SingleMapping(m)
          log.info(s"merchantToStorePairs size is now ${ValhallaApi.merchantsToStoresMap.size()}")
        }
      )
      val timeout = FiniteDuration(90, "minutes")
      Await.result(gracefulStop(router, timeout, Broadcast(PoisonPill)), timeout)
      Await.result(gracefulStop(router, timeout), timeout)
      system.terminate()
    }
    log.info(s"There are ${ValhallaApi.merchantsToStoresMap.size} merchants with stores")
    ValhallaApi.merchantsToStoresMap
  }

}

object ValhallaApi {
  var storeToMerchantMap = new ConcurrentHashMap[Int, Merchant](3000, 10, 1000)
  var merchantsToStoresMap: ConcurrentHashMap[Merchant, List[Int]] = new ConcurrentHashMap[Merchant, List[Int]](3000, 10, 1000)
}
