package StoryGenerator.Actors

import StoryBackend.{PostgresSource, StoriesDataSource}
import StoryGenerator.Actors.StoryWriter.RenderedStory
import akka.actor.{Actor, Props}
import org.apache.log4j.Logger

import scala.collection.mutable.Queue

/**
  * Created by rkilgore on 8/23/17.
  */
class StoryWriter extends Actor {
  val log = Logger.getLogger(getClass.getName)
  val storiesBackendConnection: PostgresSource = new StoriesDataSource
  val storyQueue: Queue[String] = Queue.empty[String]
  val maxQueueSize: Int = 20 //arbitrary for testing
  var producerCount: Int = 0

  def receive = {
    case RenderedStory(story) =>
      writeStory(story)
    case WriterRegistrationMessage.REGISTER =>
      producerCount += 1
      log.info(s"Registered producer, current count: $producerCount")
    case WriterRegistrationMessage.UNREGISTER =>
      log.info(s"Unregistered producer, current count: $producerCount")
      producerCount -= 1
      if( producerCount <= 0 ) {
        context.stop(self)
      }
  }

  def writeStory(story: String): Unit = {
    if (storyQueue.size >= maxQueueSize) {
      log.info(s"Writing story to postgres, queue was of size ${storyQueue.size}")
      emptyQueueToPostgres()
    }
    log.info(s"Enqueing story, queue is currently of size ${storyQueue.size}")
    storyQueue.enqueue(story)
  }

  def emptyQueueToPostgres(): Unit = {
    log.info(s"Writing queue to postgres of size ${storyQueue.size}")
    val queryStringBuilder: StringBuilder = new StringBuilder
    queryStringBuilder ++= s"INSERT INTO ${storiesBackendConnection.config.storiesTable} " +
      "(id, story_date, data, normalized_target_date, generation_date, type, sentiment, is_recalled, last_refreshed_timestamp, store_id, product_version, datasource) " +
      "VALUES " +
      s"${storyQueue.dequeue()}"

    while (storyQueue.nonEmpty) {
      queryStringBuilder ++= s", ${storyQueue.dequeue()}"
    }

    queryStringBuilder += ';'
    storiesBackendConnection.queryUpdate(queryStringBuilder.toString())
  }

  override def postStop(): Unit = {
    super.postStop()
    log.info(s"Writer shutting down per message from ${sender().toString()}")
    log.info(s"Producer count at time of shutdown is $producerCount")
    if (storyQueue.nonEmpty) {
      emptyQueueToPostgres()
    }
    log.info("Writer entering super.postStop")
  }
}

object StoryWriter {
  def props: Props = {
    Props(classOf[StoryWriter])
  }

  final case class RenderedStory(story: String)

}