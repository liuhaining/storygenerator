#Get Certs
openssl s_client -showcerts -connect repo.typesafe.com:443 </dev/null 2>/dev/null | openssl x509 -outform der > /tmp/repo.typesafe.com.der
openssl s_client -showcerts -connect repo.scala-sbt.org:443 </dev/null 2>/dev/null | openssl x509 -outform der > /tmp/repo.scala-sbt.der
openssl s_client -showcerts -connect jcenter.bintray.com:443 </dev/null 2>/dev/null | openssl x509 -outform der > /tmp/jcenter.bintray.com.der


#Add Certs to trust store (MacOS version)
keytool -import -noprompt -alias repo.typesafe.com -keystore "/docker-java-home/jre/lib/security/cacerts" -storepass "changeit" -file /tmp/repo.typesafe.com.der
keytool -import -noprompt -alias repo.scala-sbt.org -keystore "/docker-java-home/jre/lib/security/cacerts" -storepass "changeit" -file /tmp/repo.scala-sbt.der
keytool -import -noprompt -alias jcenter.bintray.com -keystore "/docker-java-home/jre/lib/security/cacerts" -storepass "changeit" -file /tmp/jcenter.bintray.com.der

