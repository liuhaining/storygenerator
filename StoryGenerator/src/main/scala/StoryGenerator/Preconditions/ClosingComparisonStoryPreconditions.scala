package StoryGenerator.Preconditions

import Merchant.Merchant

/**
  * Created by rkilgore on 9/11/17.
  */
class ClosingComparisonStoryPreconditions(initialStores: List[Int], val storyType: String = "closing-comparison")
  extends AbstractStoryPrecondition(initialStores) {
  log.debug(s"Closing comparison initial stores: ${initialStores}")

  val merchantsWithRevenueTrendsExcludingZero: List[Merchant] = storiesBackend.getMerchantsWithNonZeroRevenueVaryingSinceLastWeek()
  log.debug(s"Closing comparison merchants with no revenue=zero: ${merchantsWithRevenueTrendsExcludingZero}")

  val storesWithRevenueTrendsExcludingZero = storiesBackend.merchantsToStoreIdList(merchantsWithRevenueTrendsExcludingZero)
  log.debug(s"Closing comparison stores with no rev=zero: $storesWithRevenueTrendsExcludingZero")

  stores = stores.filter(storesWithRevenueTrendsExcludingZero.contains)
  log.debug(s"Filtered store list: $stores")
}
