package scala.StoryCard.StoryCardBuilders

import java.time.LocalDate

import StoryBackend.ValhallaApi
import StoryCard.StoryCardBuilders.{AbstractStoryBuilder, AbstractStoryCardBuilderObject, PeriodSummaryIntervalStoryBuilder}

class PeriodSummaryMonthlyStoryBuilder(storiesBackend: ValhallaApi) extends PeriodSummaryIntervalStoryBuilder(storiesBackend) {
  override val storyType: String = s"period-summary-monthly"

  override def subtractStoryIntervalFromDate(date: LocalDate): LocalDate = {
    date.minusMonths(1)
  }
}

object PeriodSummaryMonthlyStoryBuilder extends AbstractStoryCardBuilderObject {

  override def storyIsValid(storeId: Int, date: LocalDate): Boolean = {
    true
  }

  def getBuilder(storiesBackend: ValhallaApi): AbstractStoryBuilder = {
    new PeriodSummaryMonthlyStoryBuilder(storiesBackend)
  }
}