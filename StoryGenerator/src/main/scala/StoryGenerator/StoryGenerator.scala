package StoryGenerator

import java.time.LocalDate
import java.util

import scala.concurrent.Await
import scala.concurrent.duration.FiniteDuration
import org.apache.log4j.Logger
import akka.actor.{ActorRef, ActorSystem, PoisonPill}
import akka.routing.{Broadcast, RoundRobinPool}
import akka.pattern.gracefulStop
import Actors.{StoryWorker, StoryWriter, WriterRegistrationMessage}
import Actors.StoryWorker.StorySpec
import Merchant.Merchant
import StoryBackend.ValhallaApi
import Preconditions.StoryPreconditions
import Utils.StoryConfig


/**
  * Created by rkilgore on 8/21/17.
  */
class StoryGenerator(var targetDate: LocalDate) {
  val log :Logger= Logger.getLogger(getClass.getName)
  val config: StoryConfig = StoryConfig.get
  val storiesBackend = new ValhallaApi

  if (config.targetDate != null)
  {
    log.info(s"Target date override found, setting target date to ${config.targetDate}")
    targetDate = config.targetDate
  } else {
    log.info(s"No target date override found, defaulting to passed in argument: ${targetDate}")
    config.targetDate = targetDate
  }

  assert(targetDate != null)

  val storyTypes = config.enabledStories
  val numberOfWorkers: Int = config.workerPoolSize

  val system: ActorSystem = ActorSystem("StorySystem")
  val storyWriter: ActorRef = system.actorOf(StoryWriter.props)
  val storyWorkerPool: ActorRef =
    system.actorOf(StoryWorker.props(storyWriter).withRouter(RoundRobinPool(numberOfWorkers)))


  def runStoryGenerator() = {
    log.info(s"Registering workers, there should be no more than $numberOfWorkers workers")
    registerWorkersWithWriter()

    val stores: List[Int] = getStores()

    log.info(s"Target date: $targetDate")
    log.debug(s"Stores list: ${stores.toString}")

    log.info(s"Attempting to generate the following story types: ${storyTypes.toString}")

    val storyTypeToStores: util.HashMap[String, List[Int]] = buildStoryTypeToStoresMap(stores, targetDate)
    storyTypes.foreach(storyType =>
      log.info(s"Number of stores for storyType $storyType: ${storyTypeToStores.get(storyType).size}")
    )

    renderStories(storyTypes, storyTypeToStores)
    shutdownGenerator
  }

  def registerWorkersWithWriter(): Unit = {
    1 to this.numberOfWorkers foreach (i => storyWriter ! WriterRegistrationMessage.REGISTER)
  }

  def getStores(): List[Int] = {
    log.info("Getting stores")
    val merchants: List[Merchant] = storiesBackend.getMerchants
    val stores: List[Int] = storiesBackend.merchantsToStoreIdList(merchants)
    log.info(s"Initially pulling in stores of count ${stores.size} relating to merchants of count ${merchants.size}")
    stores
  }

  def buildStoryTypeToStoresMap(stores: List[Int], targetDate: LocalDate): util.HashMap[String, List[Int]] = {
    log.info("Building story type to stores map")
    log.info(s"Initial store count: ${stores.size}")
    val storyTypeToStores = new util.HashMap[String, List[Int]]()
    storyTypes.foreach(storyType =>
      storyTypeToStores
        .put(storyType, StoryPreconditions.filterOutInvalidStores(storyType, stores, targetDate))
    )
    storyTypeToStores
  }

  def renderStories(storyTypes: List[String], storyTypeToStores: util.HashMap[String, List[Int]]): Unit = {
    var id: Int = getMostRecentStoryId()
    log.info(s"Most recent story id is $id")

    storyTypes.foreach(storyType => {
      log.info(s"About to render stories for story type $storyType")
      storyTypeToStores.get(storyType)
        .foreach(store => {
          id += 1
          log.debug(s"Entering renderStory for store $store and storyType $storyType and id $id")
          renderStory(store, storyType, id)
        }
        )
    }
    )
  }

  def getMostRecentStoryId(): Int = {
    log.info("Getting most recent story id")
    storiesBackend.getMostRecentStoryId
  }

  def renderStory(store: Int, storyType: String, id: Int): Unit = {
    log.info("Sending storyspec to storyWorkerPool")
    this.storyWorkerPool ! StorySpec(store, storyType, id, targetDate)
  }

  def shutdownGenerator = {
    log.info("Entering shutdown phase")
    val timeout = FiniteDuration(10, "minutes")
    val waitTime = timeout + FiniteDuration(30, "seconds")
    log.info("Sending stop message to story workers")
    Await.result(gracefulStop(storyWorkerPool, timeout, Broadcast(PoisonPill)), waitTime)
    log.info("Sending stop message to story writer")
    Await.result(gracefulStop(storyWriter, timeout), timeout)
    log.info("Attempting system termination")
    system.terminate()
    log.info("System terminated, goodbye")
  }

}

object StoryGenerator {
  def main(args: Array[String]): Unit = {
    val log = Logger.getLogger(getClass.getName)
    var targetDate: LocalDate = null
    try {
      targetDate = LocalDate.parse(args(0))
      log.info(s"Got target date passed in through command line, value is ${targetDate.toString}")
    } catch {
      case e: ArrayIndexOutOfBoundsException =>
        log.info("No target date set, will check configs")
    }

    val generator = new StoryGenerator(targetDate)
    generator.runStoryGenerator()
  }
}