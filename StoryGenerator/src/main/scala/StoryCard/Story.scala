package StoryCard

import java.time.LocalDate

import StoryCard.StoryCardBuilders.AbstractStoryBuilder
import Utils.JsonSerializable

/**
  * Created by rkilgore on 8/15/17.
  */

class Story(builder: AbstractStoryBuilder) extends JsonSerializable {
  var id: Int = builder.id
  var storyDate: LocalDate = builder.storyDate
  var normalizedTargetDate: LocalDate = builder.normalizedTargetDate
  var generationDate: LocalDate = builder.generationDate
  var sentiment: String = builder.sentiment
  var isRecalled: Boolean = builder.isRecalled
  var lastRefreshedTimestamp: LocalDate = builder.lastRefreshedTimestamp
  var storeId: Int = builder.storeId
  var productVersion: String = builder.productVersion
  var datasource: String = builder.datasource

  var data: StoryContent = builder.data
  val storyType: String = builder.storyType

  override def toString: String = {
    s"'$id', '$storyDate', '${data.toString}' '$normalizedTargetDate', '$generationDate', '$storyType' '$sentiment', '$isRecalled', '$lastRefreshedTimestamp', '$storeId', '$productVersion', '$datasource'"
  }

  def toSqlValues: String = {
    s"('$id', DATE('$storyDate'), '${data.toString}', DATE('$normalizedTargetDate'), DATE('$generationDate'), '$storyType', '$sentiment', '$isRecalled', '$lastRefreshedTimestamp', '$storeId', '$productVersion', '$datasource')"
  }
}
