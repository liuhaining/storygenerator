-- cohort

-- non-local customer
-- customer last week / 5 weeks average , /Users/f3b8i2z/ws/git/valhalla/val-app-sbp/features/stories/generators/SegmentMetricGenerator.coffee
-- new customer last week / 5 weeks average
 select date, avg(revenue) FROM metrics where time_window='${time_window}' AND customer_class='all'
--          AND extract(dow from date) = ${dow} and
          date >= '${endDate.toString}' and date <= '${startDate.toString}' and revenue > 0
          AND merchant_id=${merchant.merchant_id} AND
          platform_id=${merchant.platform_id}
          order by date desc;

-- API /Users/f3b8i2z/ws/git/valhalla-api/app/com/palantir/valhalla/domain/dataaccess/metrics/MetricsService.scala
  /**
    * Select metrics with the specified criteria, returning empty metrics if there are no matching metrics.
    *
    * @return metrics with the specified platform, merchant, timeWindow and customerClass identifiers
    *         and with date truncated to timeWindow within closed [fromInclusive, toInclusive] interval
    */


--          notice this can be done for all in DF, do them in DF, then find 'story'
     SELECT platform_id,merchant_id, date, AVG(revenue) FROM metrics
        WHERE time_window='${time_window}' AND customer_class='all'
              AND date >= '${endDate.toString}' AND date <= '${startDate.toString}' -- they reversed it
              AND revenue > 0
              GROUP BY platform_id, merchant_id
              ORDER BY date desc;


-- PeriodBestDay
-- this one is done in new Scala code by Palantir
-- method 1
SELECT date, revenue FROM (
  SELECT date, revenue
    RANK() OVER (PARTITION BY date ORDER BY revenue DESC) revenue_rank
    WHERE customer_class='all' AND
              time_window='daily' AND
              DATE(date) >= DATE('$windowStart') AND
              DATE(date) <= DATE('$windowEnd')
                AND revenue > 0
    FROM metrics
  ) WHERE revenue_rank = 1


-- method 2
-- first create: dfMax
SELECT platform_id, merchant_id, MAX(revenue) FROM metrics
          WHERE customer_class='all' AND
              time_window='daily' AND
              DATE(date) >= DATE('$windowStart') AND
              DATE(date) <= DATE('$windowEnd')
                AND revenue > 0
              GROUP BY platform_id, merchant_id
              ORDER BY date desc;
-- df.join(dfMax, (mid, pid, revenue)).select("date", revenue))

-- schema for dfs, should it be current story card one
-- type_str,
-- INSERT INTO ${storiesBackendConnection.config.storiesTable}
    (id, STORY_DATE, DATA, normalized_target_date, generation_date, TYPE, SENTIMENT, is_recalled, last_refreshed_timestamp, STORE_ID, product_version, datasource)
